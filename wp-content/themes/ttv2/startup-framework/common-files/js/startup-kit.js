/**
 * JavaScript code for all ui-kit components.
 * Use namespaces.
 */

window.isRetina = (function() {
    var root = ( typeof exports == 'undefined' ? window : exports);
    var mediaQuery = "(-webkit-min-device-pixel-ratio: 1.5),(min--moz-device-pixel-ratio: 1.5),(-o-min-device-pixel-ratio: 3/2),(min-resolution: 1.5dppx)";
    if (root.devicePixelRatio > 1)
        return true;
    if (root.matchMedia && root.matchMedia(mediaQuery).matches)
        return true;
    return false;
})();
//nextOrFirst? prevOrLast?
jQuery.fn.nextOrFirst = function(selector) { var next = this.next(selector); return (next.length) ? next : this.prevAll(selector).last(); }
jQuery.fn.prevOrLast = function(selector){ var prev = this.prev(selector); return (prev.length) ? prev : this.nextAll(selector).last(); }

//preload images
jQuery.fn.preload=function(){this.each(function(){jQuery("<img/>")[0].src=this})}

window.startupKit = window.startupKit || {};

startupKit.hideCollapseMenu = function() {
    jQuery('body > .navbar-collapse').css({
        'z-index': 1
    });
    jQuery('html').removeClass('nav-visible');
    setTimeout(function() {
        jQuery('body > .navbar-collapse').addClass('collapse');
        jQuery('body > .colapsed-menu').removeClass('show-menu');
    }, 400)
}

jQuery(function () {
    jQuery('.page-wrapper, .navbar-fixed-top, .navbar-collapse a, .navbar-collapse button, .navbar-collapse input[type=submit]').on('click', function(e) {
        if(jQuery('html').hasClass('nav-visible')) {
            setTimeout(function(){
                startupKit.hideCollapseMenu();
            }, 200)
        }
    });
    jQuery(window).resize(function() {
        if(jQuery(window).width() > 965) {
            startupKit.hideCollapseMenu();
        }
    });

    var menuCollapse = jQuery('#header-dockbar > .colapsed-menu').clone(true);
    jQuery('body').append(menuCollapse);

    jQuery('#open-close-menu').on('click', function () {
        if(jQuery('html').hasClass('nav-visible')) {
            startupKit.hideCollapseMenu();
        } else {
            jQuery('body > .colapsed-menu').addClass('show-menu');
            if(jQuery('#header-dockbar').length) {
                 jQuery('body > .colapsed-menu').css({
                    top: jQuery('#header-dockbar').height()
                });
            }
            setTimeout(function() {
                jQuery('html').addClass('nav-visible');
            }, 1)
        }
    });
    if(jQuery('.social-btn-facebook').length){
        jQuery('.social-btn-facebook').sharrre({
            share: {
                facebook: true
            },
            enableHover: false,
            enableCounter: false,
            click: function(api, options){
                api.simulateClick();
                api.openPopup('facebook');
            }
        });
    }

    if(jQuery('.social-btn-twitter').length){
        jQuery('.social-btn-twitter').sharrre({
            share: {
                twitter: true
            },
            enableHover: false,
            enableCounter: false,
            buttons: {
                twitter: {
                    via: 'Designmodo',
                    url: false
                }
            },
            click: function(api, options){
                api.simulateClick();
                api.openPopup('twitter');
            }
        });
    }
});

/**
 *  Headers 
 * */
startupKit.uiKitHeader = startupKit.uiKitHeader || {};

startupKit.uiKitHeader._inFixedMode = function(headerClass) {
    var navCollapse = jQuery(headerClass + ' .navbar-collapse').first().clone(true);
    navCollapse.attr('id', headerClass.substr(1));
    jQuery('body').append(navCollapse);

    var fixedNavbar = jQuery('.navbar-fixed-top');
        fixedNavbarHeader = fixedNavbar.closest('header');
        fixedNavbarHeaderClone = fixedNavbarHeader.clone(true);

    if(fixedNavbarHeader.hasClass('fake-header')) {
        var fakeHeader = jQuery('<div class="fake-wrapper-header" style="width: 100%; height: ' + fixedNavbarHeader.outerHeight() + 'px;" />');
    }
    jQuery('body').prepend(fakeHeader);
    jQuery('body').prepend(fixedNavbarHeaderClone);
    fixedNavbarHeader.detach();

    jQuery(headerClass + ' .navbar-toggle').on('click', function() {
        var jQuerythis = jQuery(this);
        if(jQuery('html').hasClass('nav-visible')) {
            startupKit.hideCollapseMenu();
        } else {
            jQuery('.navbar-collapse#' + headerClass.substr(1)).removeClass('collapse');
            if(jQuery('#header-dockbar').length) {
                jQuery('.navbar-collapse#' + headerClass.substr(1)).css({
                    top: jQuery('#header-dockbar').height()
                });
            }
            setTimeout(function() {
                jQuery('html').addClass('nav-visible');
            }, 1)
            setTimeout(function() {
                jQuery('body > .navbar-collapse').css({
                    'z-index': 101
                });
            }, 400)
        }
    });

    if (jQuery(headerClass + ' .navbar').hasClass('navbar-fixed-top')) {
        var s1 = jQuery(headerClass + '-sub'),
            s1StopScroll = s1.outerHeight() - 70,
            antiflickerStopScroll = 70;

        if(jQuery(headerClass).outerHeight()>0){
            var antiflickerColor = jQuery(headerClass).css('background-color');
        }else if(jQuery(headerClass+'-sub').length > 0){
            var antiflickerColor = jQuery(headerClass+'-sub').css('background-color');
        }else{
            var antiflickerColor='#fff';
        }

        var antiflicker = jQuery('<div class="' + headerClass.slice(1) + '-startup-antiflicker header-antiflicker" style="opacity: 0.0001; position: fixed; z-index: 2; left: 0; top: 0; width: 100%; height: 70px; background-color: '+antiflickerColor+';" />');
        jQuery('body').append(antiflicker);
        var s1FadedEls = jQuery('.background, .caption, .controls > *', s1),
            header = jQuery(headerClass);

        s1FadedEls.each(function() {
            jQuery(this).data('origOpacity', jQuery(this).css('opacity'));
        });

        var headerAniStartPos = s1.outerHeight() - 120, headerAniStopPos = s1StopScroll;

        jQuery(window).scroll(function() {
            var opacity = (s1StopScroll - jQuery(window).scrollTop()) / s1StopScroll;
            opacity = Math.max(0, opacity);

            if (jQuery(window).scrollTop() > s1StopScroll - antiflickerStopScroll) {
                var opacityAntiflicker = (s1StopScroll - jQuery(window).scrollTop()) / antiflickerStopScroll;
                opacityAntiflicker = Math.max(0, opacityAntiflicker);
            } else {
                opacityAntiflicker = 1
            }
            // 0..1

            s1FadedEls.each(function() {
                jQuery(this).css('opacity', jQuery(this).data('origOpacity') * opacity);
            });

            antiflicker.css({
                'background-color': jQuery('.pt-page-current', s1).css('background-color'),
                'opacity': 1.0001 - opacityAntiflicker
            });
            
            var headerZoom = -(headerAniStartPos - jQuery(window).scrollTop()) / (headerAniStopPos - headerAniStartPos);
            headerZoom = 1 - Math.min(1, Math.max(0, headerZoom));
            
            jQuery(window).resize(function(){
               _navbarResize();
            });
            var _navbarResize = function(){
                if(jQuery(window).width()<767){
                    jQuery('.navbar', header).css({
                        'top' : -6 + ((20 + 6) * headerZoom)
                    });
                } else if(jQuery(window).width()<480){
                    jQuery('.navbar', header).css({
                        'top' : -6 + ((20 + 6) * headerZoom)
                    });
                } else{
                    jQuery('.navbar', header).css({
                        'top' : -6 + ((45 + 6) * headerZoom)
                    });
                }
            };
            
            _navbarResize();
            
            jQuery('.navbar .brand', header).css({
                'font-size' : 18 + ((25 - 18) * headerZoom),
                'padding-top' : 30 + ((23 - 30) * headerZoom)
            });
            jQuery('.navbar .brand img', header).css({
                'width' : 'auto',
                'height' : 25 + ((50 - 25) * headerZoom),
                'margin-top' : -1 + ((-10 + 1) * headerZoom)
            });
            jQuery('.navbar .btn-navbar', header).css({
                'margin-top' : 30 + ((28 - 30) * headerZoom)
            });

            if (jQuery(window).width() > 979) {
                jQuery(headerClass + '.navbar .nav > li > a', header).css({
                    'font-size' : 12 + ((14 - 12) * headerZoom)
                });
            } else {
                jQuery(headerClass + '.navbar .nav > li > a', header).css({
                    'font-size' : ''
                });
            }

        });
    };
};

/* Header 1*/
startupKit.uiKitHeader.header1 = function() {
    var pt = PageTransitions();
    pt.init('#pt-main');
    jQuery('#pt-main .control-prev').on('click', function() {
        pt.gotoPage(5, 'prev');
        return false;
    });
    jQuery('#pt-main .control-next').on('click', function() {
        pt.gotoPage(6, 'next');
        return false;
    });

    startupKit.uiKitHeader._inFixedMode('.header-1');

};

/* Header 2*/
startupKit.uiKitHeader.header2 = function() {
    startupKit.uiKitHeader._inFixedMode('.header-2');
};

/* Header 3*/
startupKit.uiKitHeader.header3 = function() {
    if (jQuery('.header-3 .navbar').hasClass('navbar-fixed-top')) {
        jQuery('.header-3').css('position', 'fixed').addClass('fake-header');
    };
    startupKit.uiKitHeader._inFixedMode('.header-3');
};

/* Header 4*/
startupKit.uiKitHeader.header4 = function() {};

/* Header 5*/
startupKit.uiKitHeader.header5 = function() {

    startupKit.uiKitHeader._inFixedMode('.header-5');
    // PageTransitions
    jQuery(window).resize(function() {
        var maxH = 0;
        jQuery('.header-5-sub .pt-page').css('height', 'auto').each(function() {
            var h = jQuery(this).outerHeight();
            if (h > maxH)
                maxH = h;
        }).css('height', maxH + 'px');
        jQuery('.header-5-sub .page-transitions').css('height', maxH + 'px');
    });
    var pt1 = PageTransitions();
    pt1.init('#h-5-pt-1');

    jQuery('#h-5-pt-1 .pt-control-prev').on('click', function() {
        pt1.gotoPage(5, 'prev');
        return false;
    });

    jQuery('#h-5-pt-1 .pt-control-next').on('click', function() {
        pt1.gotoPage(6, 'next');
        return false;
    });

    var navbar = jQuery('.header-5 .navbar');
    jQuery('.search', navbar).click(function() {
        if (!navbar.hasClass('search-mode')) {
            navbar.addClass('search-mode');
            setTimeout(function() {
                jQuery('header .navbar .navbar-search input[type="text"]').focus();
            }, 1000);
        } else {

        }
        return false;
    });

    jQuery('.close-search', navbar).click(function() {
        navbar.removeClass('search-mode');
        return false;
    });
};

/* Header 6*/
startupKit.uiKitHeader.header6 = function() {
    startupKit.uiKitHeader._inFixedMode('.header-6');
};

/* Header 7*/
startupKit.uiKitHeader.header7 = function() {
    startupKit.uiKitHeader._inFixedMode('.header-7');
    jQuery(window).resize(function() {
        var maxH = 0;
        jQuery('.header-7-sub section').css('height', jQuery(this).height() + 'px').each(function() {
            var h = jQuery(this).outerHeight();
            if (h > maxH)
                maxH = h;
        }).css('height', maxH + 'px');
        jQuery('.header-7-sub .page-transitions').css('height', maxH + 'px');
        var ctrlsHeight = jQuery('.header-7-sub .pt-controls').height();
        jQuery('.header-7-sub .pt-controls').css('margin-top', (-1) * (maxH) / 2 - ctrlsHeight + 'px');
        jQuery('.header-7-sub .pt-controls').css('padding-bottom', (maxH) / 2 - ctrlsHeight + 'px');
    });


    // PageTransitions
    var pt = PageTransitions();
    pt.init('#h-7-pt-main');

    jQuery('.header-7-sub .pt-controls .pt-indicators > *').on('click', function() {

        if (jQuery(this).hasClass('active'))
            return false;

        var curPage = jQuery(this).parent().children('.active').index();
        var nextPage = jQuery(this).index();
        jQuery('.header-7-sub').css('background-color',jQuery('#h-7-pt-main').children('.pt-page').eq(nextPage).find('section').css('background-color'));
        var ani = 5;
        if (curPage < nextPage) {
            ani = 6;
        }

        pt.gotoPage(ani, nextPage);
        jQuery(this).addClass('active').parent().children().not(this).removeClass('active');
        return false;


    });

};

/* Header 8*/
startupKit.uiKitHeader.header8 = function() {
    if (jQuery('.header-8 .navbar').hasClass('navbar-fixed-top')) {
        jQuery('.header-8').css('position', 'fixed').addClass('fake-header');
    };
    startupKit.uiKitHeader._inFixedMode('.header-8');
};

/* Header 9*/
startupKit.uiKitHeader.header9 = function() {

    startupKit.uiKitHeader._inFixedMode('.header-9');

    jQuery(window).resize(function() {
        var h = 0;
        jQuery('body > section:not(.header-9-sub)').each(function() {
            h += jQuery(this).outerHeight();
        });
        jQuery('.sidebar-content').css('height', h + 'px');
    });
};

/* Header 10*/
startupKit.uiKitHeader.header10 = function() {
    if (jQuery('.header-10 .navbar').hasClass('navbar-fixed-top')) {
        jQuery('.header-10').css('position', 'fixed').addClass('fake-header');
    };
    startupKit.uiKitHeader._inFixedMode('.header-10');

    jQuery('.header-10-sub .control-btn').on('click', function() {
        jQuery.scrollTo(jQuery(this).closest('section').next(), {
            axis : 'y',
            duration : 500
        });
        return false;
    });
};

/* Header 11*/
startupKit.uiKitHeader.header11 = function() {
    if (jQuery('.header-11 .navbar').hasClass('navbar-fixed-top')) {
        jQuery('.header-11').css('position', 'fixed').addClass('fake-header');
    };
    startupKit.uiKitHeader._inFixedMode('.header-11');

    jQuery(window).resize(function() {
        
        var headerContainer = jQuery('.header-11-sub').not('pre .header-11-sub');
        var player = headerContainer.find('.player');
        if (jQuery(window).width() < 751) {
            headerContainer.find('.signup-form').before(player);
            headerContainer.find('.player-wrapper').hide();
        } else {
            headerContainer.find('.player-wrapper').append(player).show();
        }
    });

};

/* Header 12*/
startupKit.uiKitHeader.header12 = function() {};

/* Header 13*/
startupKit.uiKitHeader.header13 = function() {};

/* Header 14*/
startupKit.uiKitHeader.header14 = function() {};

/* Header 15*/
startupKit.uiKitHeader.header15 = function() {
    if (jQuery('.header-15 .navbar').hasClass('navbar-fixed-top')) {
        jQuery('.header-15').css('position', 'fixed').addClass('fake-header');
    };
    startupKit.uiKitHeader._inFixedMode('.header-15');
};

/* Header 16*/
startupKit.uiKitHeader.header16 = function() {
    startupKit.uiKitHeader._inFixedMode('.header-16');
    var pt = PageTransitions();
    pt.init('#h-16-pt-main');
    jQuery('#h-16-pt-main .pt-control-prev').on('click', function() {
        pt.gotoPage(2, 'prev');
        return false;
    });
    jQuery('#h-16-pt-main .pt-control-next').on('click', function() {
        pt.gotoPage(1, 'next');
        return false;
    });

    jQuery('.header-16-sub .scroll-btn a').on('click', function(e) {
        e.preventDefault();
        jQuery.scrollTo(jQuery(this).closest('section').next(), {
            axis : 'y',
            duration : 500
        });
        return false;
    });
    jQuery(window).resize(function() {
        jQuery('.header-16-sub').css('height', jQuery(this).height() + 'px');
    });
    jQuery(window).resize().scroll();
};

/* Header 17*/
startupKit.uiKitHeader.header17 = function() {
    if (jQuery('.header-17 .navbar').hasClass('navbar-fixed-top')) {
        jQuery('.header-17').css('position', 'fixed').addClass('fake-header');
    };
    startupKit.uiKitHeader._inFixedMode('.header-17');
    
    var pt = PageTransitions();
    pt.init('#h-17-pt-1');

    jQuery('.pt-controls .pt-indicators > *').on('click', function() {
        if (jQuery(this).hasClass('active'))
            return false;

        var curPage = jQuery(this).parent().children('.active').index();
        var nextPage = jQuery(this).index();
        var ani = 44;
        if (curPage < nextPage) {
            ani = 45;
        }

        pt.gotoPage(ani, nextPage);
        jQuery(this).addClass('active').parent().children().not(this).removeClass('active');
        return false;
    });

    jQuery(window).resize(function() {
        jQuery('.header-17-sub .page-transitions').each(function() {
            var maxH = 0;
            jQuery('.pt-page', this).css('height', 'auto').each(function() {
                var h = jQuery(this).outerHeight();
                if (h > maxH)
                    maxH = h;
            }).css('height', maxH + 'px');
            jQuery(this).css('height', maxH + 'px');
            if(!jQuery(this).hasClass('calculated')){
                jQuery(this).addClass('calculated');
            }
        });
    });

};

/* Header 18*/
startupKit.uiKitHeader.header18 = function() {
    jQuery(window).resize(function() {
        maxH = jQuery(window).height(); 
        jQuery('.header-18 .page-transitions').css('height', maxH + 'px');
    });

    // PageTransitions
    var pt = PageTransitions();
    pt.init('#h-18-pt-main');

    jQuery('.header-18 .pt-control-prev').on('click', function() {
        pt.gotoPage(5, 'prev');
        return false;
    });

    jQuery('.header-18 .pt-control-next').on('click', function() {
        pt.gotoPage(6, 'next');
        return false;
    });

};

/* Header 19*/
startupKit.uiKitHeader.header19 = function() {
    startupKit.uiKitHeader._inFixedMode('.header-19');
};

/* Header 20*/
startupKit.uiKitHeader.header20 = function() {
    if (jQuery('.header-20 .navbar').hasClass('navbar-fixed-top')) {
        jQuery('.header-20').css('position', 'fixed').addClass('fake-header');
    };
    startupKit.uiKitHeader._inFixedMode('.header-20');
};

/* Header 21*/
startupKit.uiKitHeader.header21 = function() {
    startupKit.uiKitHeader._inFixedMode('.header-21');
    maxH = jQuery(window).height();
    if(jQuery('.navbar-fixed-top').length!=0){
        maxH = maxH - jQuery('.navbar-fixed-top').outerHeight();
    }
    if(jQuery('.header-21').length!=0){
        maxH = maxH - jQuery('.header-21').outerHeight();
    }
    if((maxH / 90) < 3){
        jQuery('.header-21-sub .control-btn').css('bottom', 0);
    }
    jQuery('.header-21-sub').height(maxH);

    jQuery('.header-21-sub .control-btn').on('click', function() {
        jQuery.scrollTo(jQuery(this).closest('section').next(), {
            axis : 'y',
            duration : 500
        });
        return false;
    });

};

/* Header 22*/
startupKit.uiKitHeader.header22 = function() {
    if (jQuery('.header-22 .navbar').hasClass('navbar-fixed-top')) {
        jQuery('.header-22').css('position', 'fixed').addClass('fake-header');
    };
    startupKit.uiKitHeader._inFixedMode('.header-22');
};

/* Header 23*/
startupKit.uiKitHeader.header23 = function() {

    startupKit.attachBgVideo();
    startupKit.uiKitHeader._inFixedMode('.header-23');

    jQuery('body').prepend(jQuery('.mask, .popup-video').not('pre .mask, pre .popup-video'));
    jQuery('header-23 .mask, header-23 .popup-video').not('pre .mask, pre .popup-video').detach();

    var iframe = jQuery('#pPlayer')[0];
    var player = jQueryf(iframe);
    player.addEvent('ready', function() {});

    function addEvent(element, eventName, callback) {
        if (element.addEventListener) {
            element.addEventListener(eventName, callback, false);
        } else {
            element.attachEvent(eventName, callback, false);
        }
    }

    jQuery('#play').on('click', function(evt) {
        evt.preventDefault();
        jQuery('.popup-video').addClass('shown');
        jQuery('.popup-video, .mask').fadeIn('slow', function() {
            player.api('play')
        });
        jQuery('.mask').on('click', function() {
            player.api('pause');
            jQuery('.popup-video, .mask').fadeOut('slow', function() {
                jQuery('.popup-video').removeClass('shown');
            });
        });
    });
};

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

/* Video background  */
startupKit.attachBgVideo = function() {
    var videBgDiv = jQuery('#bgVideo');
    if (!isMobile.any() && videBgDiv) {
        var videobackground = new jQuery.backgroundVideo(videBgDiv, {
            "holder": "#bgVideo",
            "align" : "centerXY",
            "path" : "video/",
            "width": 854,
            "height": 480,
            "filename" : "preview",
            "types" : ["mp4", "ogg", "webm"]
        });
    }
}


/**
 *  Contents 
 * */

startupKit.uiKitContent = startupKit.uiKitContent || {};


/* Content 1*/
startupKit.uiKitContent.content1 = function() {};

/* Content 2*/
startupKit.uiKitContent.content2 = function() {};

/* Content 3*/
startupKit.uiKitContent.content3 = function() {};

/* Content 4*/
startupKit.uiKitContent.content4 = function() {};

/* Content 5*/
startupKit.uiKitContent.content5 = function() {};

/* Content 6*/
startupKit.uiKitContent.content6 = function() {};

/* Content 7*/
startupKit.uiKitContent.content7 = function() {
    
    (function(el) {
        if (el.length != 0) {
            jQuery('img:first-child', el).css('left', '-29.7%');
            jQuery(window).resize(function() {
                if (!el.hasClass('ani-processed')) {
                    el.data('scrollPos', el.offset().top - jQuery(window).height() + el.outerHeight());
                }
            }).scroll(function() {
                if (!el.hasClass('ani-processed')) {
                    if (jQuery(window).scrollTop() >= el.data('scrollPos')) {
                        el.addClass('ani-processed');
                        jQuery('img:first-child', el).animate({
                            left : 0
                        }, 500);
                    }
                }
            });

        }

    })(jQuery('.screen'));

    
    

};

/* Content 8*/
startupKit.uiKitContent.content8 = function() {};

/* Content 9*/
startupKit.uiKitContent.content9 = function() {};

/* Content 10*/
startupKit.uiKitContent.content10 = function() {};

/* Content 11*/
startupKit.uiKitContent.content11 = function() {};

/* Content 12*/
startupKit.uiKitContent.content12 = function() {};

/* Content 13*/
startupKit.uiKitContent.content13 = function() {};

/* Content 14*/
startupKit.uiKitContent.content14 = function() {};

/* Content 15*/
startupKit.uiKitContent.content15 = function() {};

/* Content 16*/
startupKit.uiKitContent.content16 = function() {
    
    jQuery('.content-16 .control-btn').on('click', function() {
        jQuery.scrollTo(jQuery(this).closest('section').next(), {
            axis : 'y',
            duration : 500
        });
        return false;
    });

};

/* Content 17*/
startupKit.uiKitContent.content17 = function() {

    // Carousel auto height
    jQuery(window).resize(function() {
        jQuery('#c-17_myCarousel').each(function() {
            var maxH = 0;
            jQuery('.item', this).each(function() {
                var h = jQuery(this).outerHeight();
                if (h > maxH)
                    maxH = h;
            });
            jQuery('#c-17_myCarousel .carousel-inner', this).css('height', maxH + 'px');
        });
    });

    // Carousel start
    jQuery('#c-17_myCarousel').carousel({
        interval : 4000
    });

};

/* Content 18*/
startupKit.uiKitContent.content18 = function() {

    // Carousel auto height
    jQuery(window).resize(function() {
        jQuery('#c-18_myCarousel').each(function() {
            var maxH = 0;
            jQuery('.item', this).each(function() {
                var h = jQuery(this).outerHeight();
                if (h > maxH)
                    maxH = h;
            });
            jQuery('.carousel-inner', this).css('height', maxH + 'px');
        });
    });

    jQuery('#c-18_myCarousel').bind('slid.bs.carousel', function() {
        jQuery('.carousel-control', this).removeClass('disabled').attr('href', '#c-18_myCarousel');
        if (jQuery('.item.active', this).index() == 0) {
            jQuery('.carousel-control.left', this).addClass('disabled').attr('href', '#');
        } else if (jQuery('.item.active', this).index() == (jQuery('.item', this).length - 1)) {
            jQuery('.carousel-control.right', this).addClass('disabled').attr('href', '#');
        }
    });

};

/* Content 19*/
startupKit.uiKitContent.content19 = function() {};

/* Content 20*/
startupKit.uiKitContent.content20 = function() {};

/* Content 21*/
startupKit.uiKitContent.content21 = function() {

    jQuery(window).resize(function() {
        jQuery('.content-21 .features').each(function() {
            var maxH = 0;
            jQuery('.features-body', this).css('height', 'auto').each(function() {
                var h = jQuery(this).outerHeight();
                if (h > maxH)
                    maxH = h;
            }).css('height', maxH + 'px');
            jQuery('.features-bodies', this).css('height', maxH + 'px');
            if(!jQuery('.features-bodies', this).hasClass('calculated')){
                jQuery('.features-bodies', this).addClass('calculated');
            }
        });
    });

    jQuery('.content-21 .features .features-header .box').click(function() {
        jQuery(this).addClass('active').parent().children().not(this).removeClass('active');
        jQuery(this).closest('.features').find('.features-body').removeClass('active').eq(jQuery(this).index()).addClass('active');
        return false;
    });

};

/* Content 22*/
startupKit.uiKitContent.content22 = function() {

    (function(el) {
        if (isRetina) {
            jQuery('.img img', el).each(function() {
                jQuery(this).attr('src', jQuery(this).attr('src').replace(/.png/i, '@2x.png'));
            });
        }

        jQuery(window).resize(function() {
            if (!el.hasClass('ani-processed')) {
                el.data('scrollPos', el.offset().top - jQuery(window).height() + el.outerHeight() - parseInt(el.css('padding-bottom'), 10));
            }
        }).scroll(function() {
            if (!el.hasClass('ani-processed')) {
                if (jQuery(window).scrollTop() >= el.data('scrollPos')) {
                    el.addClass('ani-processed');
                }
            }
        });
    })(jQuery('.content-22'));

};
/* Content 23*/
startupKit.uiKitContent.content23 = function() {

    jQuery('.content-23 .control-btn').on('click', function() {
        jQuery.scrollTo(jQuery(this).closest('section').next(), {
            axis : 'y',
            duration : 500
        });
        return false;
    });

};
/* Content 24*/
startupKit.uiKitContent.content24 = function() {

    jQuery(window).resize(function() {
        jQuery('.content-24 .features').each(function() {
            var maxH = 0;
            jQuery('.features-body', this).css('height', 'auto').each(function() {
                var h = jQuery(this).outerHeight();
                if (h > maxH)
                    maxH = h;
            }).css('height', maxH + 'px');
            jQuery('.features-bodies', this).css('height', maxH + 'px');
        });
    });

    jQuery('.content-24 .features .features-header .box').click(function() {
        jQuery(this).addClass('active').parent().children().not(this).removeClass('active');
        jQuery(this).closest('.features').find('.features-body').removeClass('active').eq(jQuery(this).index()).addClass('active');
        return false;
    });

};
/* Content 25*/
startupKit.uiKitContent.content25 = function() {

    if ((!document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image", "1.1")) || (window.mobile)) {
        jQuery('.svg').remove();
        jQuery('.nosvg').attr('style', 'display:block;');
    }

    (function(el) {
        el.css('opacity', 0);
        jQuerysvg = jQuery('#spaceship', el);
        jQuery('#rocket-raw', jQuerysvg).attr('transform', 'translate(-100,100)');
        jQuery('#rocketmask1', jQuerysvg).attr('transform', 'translate(100,-100)');

        jQuery(window).resize(function() {
            if (!el.hasClass('ani-processed')) {
                el.data('scrollPos', el.offset().top - jQuery(window).height() + el.outerHeight());
            }
            var svg = jQuery('.content-25 .svg');
            var nosvg = jQuery('.content-25 .nosvg');
            jQuery('.content-25 .col-sm-6:nth-child(2)').show();
            jQuery('.content-25 .col-sm-6:nth-child(2)').append(nosvg);
        }).scroll(function() {
            if (!el.hasClass('ani-processed')) {
                if (jQuery(window).scrollTop() >= el.data('scrollPos')) {
                    el.addClass('ani-processed');
                    el.animate({
                        opacity : 1
                    }, 600);
                    jQuery('#rocket-raw, #rocketmask1', jQuerysvg).clearQueue().stop().animate({
                        svgTransform : 'translate(0,0)'
                    }, {
                        duration : 800,
                        easing : "easeInOutQuad"
                    });
                }
            }
        });
    })(jQuery('.content-25 .col-sm-6 + .col-sm-6'));

};
/* Content 26*/
startupKit.uiKitContent.content26 = function() {};

/* Content 27*/
startupKit.uiKitContent.content27 = function() {
    if ((!document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image", "1.1")) || (window.mobile)) {
        jQuery('.svg').remove();
        jQuery('.nosvg').attr('style', 'display:block;');
    }

    jQuery(window).resize(function() {
        var svg = jQuery('.content-27 .svg');
        var nosvg = jQuery('.content-27 .nosvg');
        jQuery('.content-27 .col-sm-4:first-child').show();
        jQuery('.content-27 .col-sm-4:first-child').append(nosvg);
    });
};
/* Content 28*/
startupKit.uiKitContent.content28 = function() {};
/* Content 29*/
startupKit.uiKitContent.content29 = function() {};
/* Content 30*/
startupKit.uiKitContent.content30 = function() {

    jQuery(window).resize(function() {
        var boxes = jQuery('.content-30 .col-sm-3');
        for (var t = 0; t <= boxes.length; t++) {
            var descTop = jQuery(boxes[t]).find('.description-top');
            if (jQuery(window).width() <= 480) {
                jQuery(boxes[t]).find('.img').after(descTop);

            } else {
                jQuery(boxes[t]).find('.img').before(descTop);
            }
        }
    });

};
/* Content 31*/
startupKit.uiKitContent.content31 = function() {
    (function(el) {
        jQuery(window).scroll(function() {
            if (jQuery(window).width() > 480) {
                jQuery('.row', el).each(function(idx) {
                    if (jQuery(window).scrollTop() >= (jQuery(this).offset().top - jQuery(window).height() + jQuery(window).height() / 2 + 100)) {
                        jQuery(this).addClass('active');
                    } else {
                        jQuery(this).removeClass('active');
                    }
                });
            }
        });
        jQuery(window).resize(function() {
            jQuery('.page-transitions', el).each(function() {
                var maxH = 0;
                jQuery('.pt-page', this).css('height', 'auto').each(function() {
                    var h = jQuery(this).outerHeight();
                    if (h > maxH)
                        maxH = h;
                }).css('height', maxH + 'px');
                jQuery(this).css('height', maxH + 'px');
            });
        });
        jQuery('.page-transitions', el).each(function() {
            var pt = PageTransitions();
            pt.init(this);

            jQuery('.pt-control-prev', this).on('click', function() {
                pt.gotoPage(68, 'prev');
                return false;
            });

            jQuery('.pt-control-next', this).on('click', function() {
                pt.gotoPage(68, 'next');
                return false;
            });
        });
    })(jQuery('.content-31'));
};

/* Content 32*/
startupKit.uiKitContent.content32 = function() {}

/* Content 33*/
startupKit.uiKitContent.content33 = function() {}

/* Content 34*/
startupKit.uiKitContent.content34 = function() {
    jQuery(window).resize(function() {
        var maxH = 0;
        jQuery('.content-34 section').each(function() {
            var h = jQuery(this).outerHeight();
            if (h > maxH)
                maxH = h;
        });
        jQuery('.content-34 .page-transitions').css('height', maxH + 'px');
        var ctrlsHeight = jQuery('.content-34 .pt-controls').height();
        jQuery('.content-34 .pt-controls').css('margin-top', (-1) * ctrlsHeight / 2 + 'px');
    });
    // PageTransitions
    var pt = PageTransitions();
    pt.init('#content-34-pt-main');
    jQuery('.content-34 .pt-controls .pt-indicators > *').on('click', function() {
        if (jQuery(this).hasClass('active'))
            return false;
        var curPage = jQuery(this).parent().children('.active').index();
        var nextPage = jQuery(this).index();
        var ani = 5;
        if (curPage < nextPage) {
            ani = 6;
        }
        pt.gotoPage(ani, nextPage);
        jQuery(this).addClass('active').parent().children().not(this).removeClass('active');        
        return false;
    });
}

/* Content 35*/
startupKit.uiKitContent.content35 = function() {
    if(jQuery(".content-35-slider").length) {
        jQuery('.content-35-slider').bxSlider({
            'controls': false,
            'pagerCustom': '.content-35-customPager',
            'adaptiveHeight': false,
            'infiniteLoop': false
        });
    }
    var pager = jQuery('.content-35-customPager');
    pager.find(jQuery('.menuicon')).on('mouseenter', function(){
        jQuery(this).parent().addClass('showmenu');
    })
    pager.on('mouseleave', function(){
        jQuery(this).removeClass('showmenu');
    })
    pager.find(jQuery('.menuicon')).on('click', function(){
        var menu = jQuery(this).parent();
        if(menu.hasClass('showmenu')) {
            menu.removeClass('showmenu');
        } else {
            menu.addClass('showmenu');
        }
    })
}

/* Content 36*/
startupKit.uiKitContent.content36 = function() {}
/* Content 37*/
startupKit.uiKitContent.content37 = function() {}
/* Content 38*/
startupKit.uiKitContent.content38 = function() {
    //samples grid
    var samplesGrid = jQuery('.samples-grid');
    setTimeout(function () {
        samplesGrid.masonry({itemSelector: '.sample-box'});
    }, 3000);

    //show images on scroll
    if (!isMobile.any()) {
        jQuery(document).scroll(function () {
            var samples = jQuery('.samples-holder');
            var samplesCont = samples.offset();
            var scrollTop = jQuery(document).scrollTop();
            if (scrollTop >= samplesCont.top - jQuery(window).height() + 200) {
                samplesGrid.masonry({itemSelector: '.sample-box'})
            }

            //samples
            if (jQuery(document).scrollTop() >= samplesCont.top - jQuery(window).height() / 2) {
                if (!jQuery('.samples-holder').hasClass('shown')) {
                    if (!isMobile.any()) {
                        addSamplesInt = setInterval(function () {
                            var elements = jQuery('.sample-box:not(.visible)');
                            var random = Math.round(Math.random() * elements.size());
                            var el = elements.eq(random);
                            el.addClass('visible');
                            if (elements.size() == 0) {
                                clearInterval(addSamplesInt);
                            }
                        }, 50);
                    }
                    jQuery('.samples-holder').addClass('shown');
                }
            }
        });
    }
    else{
        jQuery('.samples-holder').addClass('shown');
        jQuery('.sample-box').addClass('visible');
    }
    //can I see the real pixels?
    jQuery('.samples-holder img').click(function () {
        var imgsrc = jQuery(this).attr('src');
        var file = imgsrc.split('/');
        var filename = file[file.length - 1];
        var structure = jQuery(this).data('structure');
        var path = imgsrc.split('/' + filename);
        path = path[0];
        showLargeImage(filename, path + '-large/', jQuery(this), 'next', structure);
    });

    if (window.location.hash.indexOf(".samples-holder") != -1) {
        var id = window.location.hash;
        jQuery(id).click();
    }

    jQuery(document).keydown(function (e) {
        if (e.keyCode == 37) {
            jQuery('.largeScreenshots .prev').click();
            return false;
        }
        if (e.keyCode == 39) {
            jQuery('.largeScreenshots .next').click();
            return false;
        }
        if (e.keyCode == 38) {
            jQuery('.largeScreenshots').clearQueue().animate({ scrollTop: jQuery('.largeScreenshots').scrollTop() - 500 + "px"}, 250);
            return false;
        }
        if (e.keyCode == 40) {
            jQuery('.largeScreenshots').clearQueue().animate({ scrollTop: jQuery('.largeScreenshots').scrollTop() + 500 + "px"}, 250);
            return false;
        }
        if (e.keyCode == 27) {
            jQuery('.close').click();
            return false;
        }
    });

    function showLargeImage(file, prefix, obj, direction, structure) {

        //dark screen, add elements
        if (!jQuery('body').hasClass('largescreenshotsopened')) {
            jQuery('body').addClass('noscroll').addClass('largescreenshotsopened').append('<div class="largeScreenshots"><div class="picHolder"><h1></h1><span></span><div class="imgHolder"><img/></div></div><div class="prev"></div><div class="next"></div><div class="close"></div></div>');
            jQuery('.largeScreenshots .close, .largeScreenshots span').click(function (e) {
                jQuery('body').removeClass('noscroll').removeClass('largescreenshotsopened');
                jQuery('.largeScreenshots').remove();
                window.location.hash = "/";
            });
        }

        //show me the image
        jQuery('.largeScreenshots .imgHolder img').attr('src', prefix + file);
        jQuery('.largeScreenshots .imgHolder img').ready(function (e) {
            jQuery('.largeScreenshots').scrollTop(0);
            jQuery('.largeScreenshots .imgHolder img');
            jQuery('.largeScreenshots h1').text(obj.attr('alt'));

            window.location.hash=obj.attr('id');

            var speed = '0.75s cubic-bezier(.27,1.64,.32,.95)';
            jQuery('.picHolder, .picHolder h1').css('-webkit-animation', direction + " " + speed).css('-moz-animation', direction + " " + speed).css('-ms-animation', direction + " " + speed).css("-o-animation", direction + " " + speed).css("animation", direction + " " + speed);
            setTimeout(function () {
                jQuery('.picHolder, .picHolder h1').removeAttr('style');
            }, 750);
        });

        //set nice position for arrows

        function setNicePosition(){
            var p = jQuery(".largeScreenshots .picHolder");
            var position = p.position();
            var size = jQuery('.largeScreenshots img').outerHeight();
            var scrolltop = jQuery(".largeScreenshots").scrollTop()
            if (position.top+192> 0) {
                jQuery('.largeScreenshots .prev, .largeScreenshots .next').css('top', position.top+192).css('height', jQuery(window).height() - position.top  - 192);
            } else if (scrolltop + jQuery(window).height() > size+192+36) {
                var posFromBottom = (scrolltop + jQuery(window).height()) - (size+192+36);
                jQuery('.largeScreenshots .prev, .largeScreenshots .next').css('top', 0).css('height', jQuery(window).height()-posFromBottom);
            } else {
                jQuery('.largeScreenshots .prev, .largeScreenshots .next').css('top', 0).css('height', jQuery(window).height());
            }
        }
        setNicePosition()

        jQuery('.largeScreenshots').scroll(function () {
            setNicePosition();
        });

        //preload pics
        var newObj = obj.parent().nextOrFirst('.samples-holder .sample-box').find('img');
        var imgsrc = newObj.attr('src');
        var file = imgsrc.split('/');
        var filename = file[file.length - 1];
        var path = imgsrc.split('/' + filename);
        path = path[0];
        jQuery([path + '-large/' + filename]).preload();

        var newObj = obj.parent().prevOrLast('.samples-holder .sample-box').find('img');
        var imgsrc = newObj.attr('src');
        var file = imgsrc.split('/');
        var filename = file[file.length - 1];
        var path = imgsrc.split('/' + filename);
        path = path[0];
        jQuery([path + '-large/' + filename]).preload();

        //get next picure and show next
        jQuery('.largeScreenshots .prev,.largeScreenshots .next, .largeScreenshots .imgHolder img').unbind();
        setTimeout(function () {
            jQuery('.largeScreenshots .prev').click(function () {
                var newObj = obj.parent().prevOrLast('.samples-holder .sample-box').find('img');
                var structure = obj.data('structure');
                var imgsrc = newObj.attr('src');
                var file = imgsrc.split('/');
                var filename = file[file.length - 1];
                var path = imgsrc.split('/' + filename);
                path = path[0];

                showLargeImage(filename, path + '-large/', newObj, "prev",structure);
            });

            jQuery('.largeScreenshots .next, .largeScreenshots .imgHolder img').click(function () {

                var newObj = obj.parent().nextOrFirst('.samples-holder .sample-box').find('img');
                var structure = newObj.data('structure');
                var imgsrc = newObj.attr('src');
                var file = imgsrc.split('/');
                var filename = file[file.length - 1];
                var path = imgsrc.split('/' + filename);
                path = path[0];

                showLargeImage(filename, path + '-large/', newObj, "next",structure);
            });
        },750);

        //add swipe gesture for mobile
         if (isMobile.any()){
             jQuery('.largeScreenshots .imgHolder img').touchwipe({
                  wipeLeft: function() { jQuery('.largeScreenshots .next').click(); },
                 wipeRight: function(){ jQuery('.largeScreenshots .prev').click(); },
                 min_move_x: 20,
                  min_move_y: 20,
                 preventDefaultEvents: false
             });
         }
    }
};


/**
 * Blogs 
 */

startupKit.uiKitBlog = startupKit.uiKitBlog || {};

/* Blog 1*/
startupKit.uiKitBlog.blog1 = function() {

    jQuery(window).resize(function() {
        jQuery('.page-transitions').each(function() {
            var maxH = 0;
            jQuery('.pt-page', this).css('height', 'auto').each(function() {
                var h = jQuery(this).outerHeight();
                if (h > maxH)
                    maxH = h;
            }).css('height', maxH + 'px');
            jQuery(this).css('height', maxH + 'px');
        });
    });

    var pt1 = PageTransitions();
    pt1.init(jQuery('#b1-pt-1'));

    jQuery('#b1-pt-1 .pt-control-prev').on('click', function() {
        pt1.gotoPage(28, 'prev');
        return false;
    });

    jQuery('#b1-pt-1 .pt-control-next').on('click', function() {
        pt1.gotoPage(29, 'next');
        return false;
    });

};

/* Blog 2*/
startupKit.uiKitBlog.blog2 = function() {};
/* Blog 3*/
startupKit.uiKitBlog.blog3 = function() {};
/* Blog 4*/
startupKit.uiKitBlog.blog4 = function() {};
/* Blog 5*/
startupKit.uiKitBlog.blog5 = function() {

    var pt2 = PageTransitions();
    pt2.init(jQuery('#b5-pt-2'));

    jQuery('#b5-pt-2 .pt-control-prev').on('click', function() {
        pt2.gotoPage(28, 'prev');
        return false;
    });

    jQuery('#b5-pt-2 .pt-control-next').on('click', function() {
        pt2.gotoPage(29, 'next');
        return false;
    });

};

/**
 * Crews 
 */

startupKit.uiKitCrew = startupKit.uiKitCrew ||
function() {
    jQuery('.member .photo img').each(function() {
        jQuery(this).hide().parent().css('background-image', 'url("' + this.src + '")');
    });
};


/**
 * Projects 
 */
startupKit.uiKitProjects = startupKit.uiKitProjects || {};

/* project-1 */
startupKit.uiKitProjects.project1 = function() {};

/* project-2 */
startupKit.uiKitProjects.project2 = function() {};

/* project-3 */
startupKit.uiKitProjects.project3 = function() {};

/* project-4 */
startupKit.uiKitProjects.project4 = function() {
    jQuery('.overlay').on('hover', function() {
        jQuery(this).closest('.project').find('.name').toggleClass('hover');
    });
};



/**
 * Footers 
 */
startupKit.uiKitFooter = startupKit.uiKitFooter || {};

/* Footer 1*/
startupKit.uiKitFooter.footer1 = function() {};

/* Footer 2*/
startupKit.uiKitFooter.footer2 = function() {};

/* Footer 3*/
startupKit.uiKitFooter.footer3 = function() {};

/* Footer 4*/
startupKit.uiKitFooter.footer4 = function() {};

/* Footer 5*/
startupKit.uiKitFooter.footer5 = function() {};

/* Footer 6*/
startupKit.uiKitFooter.footer6 = function() {};

/* Footer 7*/
startupKit.uiKitFooter.footer7 = function() {};

/* Footer 8*/
startupKit.uiKitFooter.footer8 = function() {};

/* Footer 9*/
startupKit.uiKitFooter.footer9 = function() {};

/* Footer 10*/
startupKit.uiKitFooter.footer10 = function() {};

/* Footer 11*/
startupKit.uiKitFooter.footer11 = function() {};

/* Footer 12*/
startupKit.uiKitFooter.footer12 = function() {};

/* Footer 13*/
startupKit.uiKitFooter.footer13 = function() {};

/* Footer 14*/
startupKit.uiKitFooter.footer14 = function() {};

/* Footer 15*/
startupKit.uiKitFooter.footer15 = function() {};


/** 
 * Global part of startup-kit
 * */
(function(jQuery) {
    jQuery(function() {
        /* implementing headers */
        for (header in startupKit.uiKitHeader) {
            headerNumber = header.slice(6);
            if (jQuery('.header-' + headerNumber).length != 0) {
                startupKit.uiKitHeader[header]();
            };
        }

        /* implementing contents */
        for (content in startupKit.uiKitContent) {
            contentNumber = content.slice(7);
            if (jQuery('.content-' + contentNumber).length != 0) {
                startupKit.uiKitContent[content]();
            };
        }
        
        /* implementing blogs */
        for (blog in startupKit.uiKitBlog) {
            blogNumber = blog.slice(4);
            if (jQuery('.blog-' + blogNumber).length != 0) {
                startupKit.uiKitBlog[blog]();
            };
        }
        
        /* implementing projects */
        for (project in startupKit.uiKitProjects) {
            projectNumber = project.slice(7);
            if (jQuery('.projects-' + projectNumber).length != 0) {
                startupKit.uiKitProjects[project]();
            };
        }

        /* implementing crew */
        startupKit.uiKitCrew();

        /* implementing footers */
        for (footer in startupKit.uiKitFooter) {
            footerNumber = footer.slice(6);
            if (jQuery('.footer-' + footerNumber).length != 0) {
                startupKit.uiKitFooter[footer]();
            };
        }
    
        /* function on load */
        jQuery(window).load(function() {
            jQuery('html').addClass('loaded');
            jQuery(window).resize();
        });

        /* ie fix images */
        if (/msie/i.test(navigator.userAgent)) {
            jQuery('img').each(function() {
                jQuery(this).css({
                    width : jQuery(this).attr('width') + 'px',
                    height : 'auto'
                });
            });
        }

        // Focus state for append/prepend inputs
        jQuery('.input-prepend, .input-append').on('focus', 'input', function() {
            jQuery(this).closest('.control-group, form').addClass('focus');
        }).on('blur', 'input', function() {
            jQuery(this).closest('.control-group, form').removeClass('focus');
        });

        // replace project img to background-image
        jQuery('.project .photo img').each(function() {
            jQuery(this).hide().parent().css('background-image', 'url("' + this.src + '")');
        });

        // Tiles
        var tiles = jQuery('.tiles');

        // Tiles phone mode
        jQuery(window).resize(function() {
            if (jQuery(this).width() < 768) {
                if (!tiles.hasClass('phone-mode')) {
                    jQuery('td[class*="tile-"]', tiles).each(function() {
                        jQuery('<div />').addClass(this.className).append(jQuery(this).contents()).appendTo(tiles);
                    });

                    tiles.addClass('phone-mode');
                }
            } else {
                if (tiles.hasClass('phone-mode')) {
                    jQuery('> [class*="tile-"]', tiles).each(function(idx) {
                        jQuery('td[class*="tile-"]', tiles).eq(idx).append(jQuery(this).contents());
                        jQuery(this).remove();
                    });

                    tiles.removeClass('phone-mode');
                }
            }
        });

        tiles.on('mouseenter', '[class*="tile-"]', function() {
            jQuery(this).removeClass('faded').closest('.tiles').find('[class*="tile-"]').not(this).addClass('faded');
        }).on('mouseleave', '[class*="tile-"]', function() {
            jQuery(this).closest('.tiles').find('[class*="tile-"]').removeClass('faded');
        });
        
        
    });
    //add some smooth for scroll


})(jQuery);
//swipe
(function(jQuery){jQuery.fn.touchwipe=function(settings){var config={min_move_x:20,min_move_y:20,wipeLeft:function(){},wipeRight:function(){},wipeUp:function(){},wipeDown:function(){},preventDefaultEvents:true};if(settings)jQuery.extend(config,settings);this.each(function(){var startX;var startY;var isMoving=false;function cancelTouch(){this.removeEventListener('touchmove',onTouchMove);startX=null;isMoving=false}function onTouchMove(e){if(config.preventDefaultEvents){e.preventDefault()}if(isMoving){var x=e.touches[0].pageX;var y=e.touches[0].pageY;var dx=startX-x;var dy=startY-y;if(Math.abs(dx)>=config.min_move_x){cancelTouch();if(dx>0){config.wipeLeft();e.preventDefault()}else{config.wipeRight();e.preventDefault()}}else if(Math.abs(dy)>=config.min_move_y){cancelTouch();if(dy>0){config.wipeDown()}else{config.wipeUp()}}}}function onTouchStart(e){if(e.touches.length==1){startX=e.touches[0].pageX;startY=e.touches[0].pageY;isMoving=true;this.addEventListener('touchmove',onTouchMove,false)}}if('ontouchstart'in document.documentElement){this.addEventListener('touchstart',onTouchStart,false)}});return this}})(jQuery);
