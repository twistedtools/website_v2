<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Twisted Tools v2
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />
		
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?> data-spy="scroll">
		<div class="page-wrapper">
			<header class="header-2">
				<div class="container">
					<div class="row">
      			<?php get_template_part('partials/header', 'nav'); ?>
      		</div>
      	</div>
	    </header>