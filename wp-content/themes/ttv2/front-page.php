<?php
/**
 * The template for displaying the front page.
 *
 * Description here
 *
 * @package Twisted Tools v2
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php get_template_part('partials/home', 'jumbotron'); ?>
		<?php get_template_part('partials/section', 'projects4'); ?>
		<?php get_template_part('partials/section', 'content36'); ?>
		<?php get_template_part('partials/section', 'content5'); ?>
		<?php get_template_part('partials/section', 'email'); ?>


	</main>
</div>

<?php get_footer(); ?>