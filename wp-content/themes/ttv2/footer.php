<?php
/**
 * The template for displaying the footer.
 *
 * @package Twisted Tools v2
 */
?>
      <?php 
        if (is_front_page()) {
          $footer_template = "frontpage";
        } else {
          $footer_template = "default";
        }

        get_template_part('partials/footer', $footer_template); 
      ?>

    </div><!--/ .page-wrapper -->

    <?php wp_footer(); ?>

  </body>
</html>