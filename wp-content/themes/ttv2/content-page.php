<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Twisted Tools v2
 */
?>
	<?php echo the_content(); ?>