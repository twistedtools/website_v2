<?php
/**
 * Twisted Tools v2 Theme Setup
 *
 * @package Twisted Tools v2
 */

//CTA Shortcode
// Add Shortcode
/*
[basic_cta heading='I am a heading' text='texty text' image_url='' orientation='rtl' enable_primary_button=1 primary_button_text='Primary' primary_button_url='#' primary_button_style='btn-primary' primary_button_fill='' enable_secondary_button=1 secondary_button_text='Primary' secondary_button_url='#' secondary_button_style='btn-default' secondary_button_fill='']

*/
function nothing_to_show_shortcode( $atts , $content = null ) {

  // Attributes
  extract( shortcode_atts(
    array(
    ), $atts )
  );
?>
<section class="content-7 v-center">
  <div>
    <div class="container">
      <h3>Nothing to Show</h3>
    </div>
  </div>
</section>
<?php 
}
add_shortcode('nothing_to_show', 'nothing_to_show_shortcodee');

function audio_button_shortcode( $atts , $content = null ) {

  // Attributes
  extract( shortcode_atts(
    array(
      'url' => '',
      'title' => ''
    ), $atts )
  );
  $jsnippet="jQuery.fullwidthAudioPlayer.addTrack('".$url."', '".$title."', '', '', '', true);return false;";

?>
<a href="" onclick="<?php echo $jsnippet; ?>">play <?php echo $title; ?></a>

<?php 
}
add_shortcode('audio_button', 'audio_button_shortcode');


/*
function basic_cta_shortcode( $atts , $content = null ) {

  // Attributes
  extract( shortcode_atts(
    array(
      'heading' => 'I am a heading',
      'text' => 'texty text',
      'image_url' => '',
      'orientation' => 'rtl',
      'enable_primary_button' => 1,
      'primary_button_text' => 'Primary',
      'primary_button_url' => '#',
      'primary_button_style' => 'btn-primary',
      'primary_button_fill' => '',
      'enable-secondary_button' => 1,
      'secondary-button_text' => 'Primary',
      'secondary-button_url' => '#',
      'secondary-button_style' => 'btn-default',
      'secondary-button_fill' => ''
    ), $atts )
  );
  if ($enable_primary_button == 1) {
    $primary_button_classes = $primary_button_style;
    if ($primary_button_fill != '') {
      $primary_button_classes = $primary_button_classes.' '.$primary_button_fill;
    }
  }
  if ($enable_secondary_button == 1) {
    $secondary_button_classes = $secondary_button_style;
    if ($secondary_button_fill != '') {
      $secondary_button_classes = $secondary_button_classes.' '.$secondary_button_fill;
    }
  }
  if (($orientation != 'rtl') && ($orientation != 'ltr')) {
    $orientation = 'rtl';
    $content_style="direction:".$orientation.";";
    $primary_button_classes = $primary_button_style;
    if ($primary_button_fill != '') {
      $primary_button_classes = $primary_button_classes.' '.$primary_button_fill;
    }
  }

?>
<section class="content-2" style="<?php echo $content_style ?>">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <h3><?php echo $heading ?>/h3>
        <p><?php echo $text ?>
        </p>
        <div class="btns">
          <?php 
          if ($enable_primary_button == 1) { ?>
            <a class="btn <?php echo $primary_button_classes ?>" href="<?php echo $primary_button_url ?>"><?php echo $primary_button_text ?></a>
          <?php } 
          if ($enable_secondary_button == 1) { ?>
            <a class="btn <?php echo $secondary_button_classes ?>" href="<?php echo $secondary_button_url ?>"><?php echo $secondary_button_text ?></a>
          <?php } ?>
        </div>
      </div>
      <div class="col-sm-5 col-sm-offset-1">
        <?php if ($image_url != '') { ?>
          <div class="img">
            <img src="<?php echo $image_url ?>" alt="">
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</section>
<?php 
}
add_shortcode('basic_cta', 'basic_cta_shortcode'); */