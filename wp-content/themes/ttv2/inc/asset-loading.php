<?php
/**
 * Asset Loading
 *
 * @package Twisted Tools v2
 */

//Global Variables
$cache_ver = "0.1.1";

if ( ! function_exists( 'load_styles' ) ) :
	// Load Theme Styles
	function load_styles() {
		global $cache_ver;
		$css_dir="/css";
		$startup_dir="/startup-framework";
		$flatui_dir="/flat-ui";
		$uikit_dir="/ui-kit";
		$bootstrap_dir="/bootstrap";
		$commonfiles_dir="/common-files";
		$flatuicss_path=$startup_dir.$flatui_dir.$css_dir;
		$commonfilescss_path=$startup_dir.$commonfiles_dir.$css_dir;
		$uikit_path=$startup_dir.$uikit_dir;
		/*
		$bootstrap_cdn_uri="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"; */

		// Theme Base & Reset CSS
	  wp_enqueue_style("style", get_template_directory_uri()."/style.css", false, $cache_ver);

	  do_action("queue_fonts");
	  // Bootstrap
	  wp_enqueue_style("bootstrap-min", get_template_directory_uri().$startup_dir.$flatui_dir.$bootstrap_dir.$css_dir."/bootstrap.css", array("style"), $cache_ver);
	  // Flat UI
		wp_enqueue_style("flat-ui", get_template_directory_uri().$startup_dir.$flatui_dir.$css_dir."/flat-ui.css", array("style", "bootstrap-min"), $cache_ver);
		// Assets for Startup Framework
		wp_enqueue_style("icon-font", get_template_directory_uri().$commonfilescss_path."/icon-font.css", array("style", "bootstrap-min", "flat-ui"), $cache_ver);
		wp_enqueue_style("animations", get_template_directory_uri().$commonfilescss_path."/animations.css", array("style", "bootstrap-min", "flat-ui"), $cache_ver);
		// Load UI Kits (all for now)
		$uikits_to_load = array("header", "content", "blog", "contacts", "crew", "price", "projects", "footer");
		foreach($uikits_to_load as $kit) {
			wp_enqueue_style("uikit-".$kit, get_template_directory_uri().$uikit_path."/ui-kit-".$kit."/css/style.css", array("style", "bootstrap-min", "flat-ui", "icon-font", "animations"), $cache_ver);
		}
		// Startup Framework Base Theme CSS
		wp_enqueue_style("startup-theme", get_template_directory_uri().$css_dir."/startup-theme.css", array("style", "bootstrap-min", "flat-ui", "icon-font", "animations", "uikit-footer"), $cache_ver);
		// Twisted Tools Main Stylesheet
		wp_enqueue_style("application", get_template_directory_uri().$css_dir."/application.css", array("style", "bootstrap-min", "flat-ui", "icon-font", "animations", "uikit-footer", "startup-theme"), $cache_ver);
		wp_enqueue_style("font-awesome", get_template_directory_uri().$css_dir."/font-awesome.min.css", array("application"), $cache_ver);
		
		wp_enqueue_style("nivo-lightbox", get_template_directory_uri().$css_dir."/nivo-lightbox.css", array("application"), $cache_ver);		
	}
endif;
add_action( 'wp_enqueue_scripts', 'load_styles' );

if ( ! function_exists( 'load_scripts' ) ) :
	// Load Theme Scripts
	function load_scripts() {
		global $cache_ver;
		$js_dir="/js";
		$startup_dir="/startup-framework";
		$commonfiles_dir="/common-files";
		$bootstrap_dir="/bootstrap";
		$flatui_dir="/flat-ui";
		$flatui_path=$startup_dir.$flatui_dir.$js_dir;
		$commonjs_path=$startup_dir.$commonfiles_dir.$js_dir;
		$jquery_cdn_uri="https://code.jquery.com/jquery-2.1.1.min.js";
		$bootstrap_cdn_uri="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js";
		// Jquery CDN
		wp_dequeue_script("jquery");
		wp_dequeue_script("jquery-migrate");
		wp_deregister_script('jquery');
		wp_register_script("jquery", get_template_directory_uri().$commonjs_path."/jquery-1.10.2.min.js", false, false, true);
		wp_enqueue_script("jquery");
		// Jquery Plugins
		$jqueryplugins_to_load = array("svg", "svganim", "parallax-min");
		foreach($jqueryplugins_to_load as $plugin) {
			$dotted_name = preg_replace("/-/", "/./", $plugin);
			wp_enqueue_style("jquery-".$plugin, get_template_directory_uri().$commonjs_path."/jquery.".$dotted_name.".js", array("jquery"), $cache_ver, true);
		}
		// Bootstrap CDN
		wp_enqueue_script("bootstrap", get_template_directory_uri().$flatui_path."/bootstrap.min.js", array("jquery"), $cache_ver, true);
		// Modernizr
		wp_enqueue_script("modernizr", get_template_directory_uri().$commonjs_path."/modernizr.custom.js", array("jquery", "bootstrap"), $cache_ver, true);
		// Misc Assets
		wp_enqueue_script("page-transitions", get_template_directory_uri().$commonjs_path."/page-transitions.js", array("jquery", "bootstrap", "modernizr"), $cache_ver, true);
		wp_enqueue_script("easing-min", get_template_directory_uri().$commonjs_path."/easing.min.js", array("jquery", "bootstrap", "modernizr"), $cache_ver, true);
		wp_enqueue_script("sharre", get_template_directory_uri().$commonjs_path."/jquery.sharrre.min.js", array("jquery", "bootstrap", "modernizr"), $cache_ver, true);

		// Startupkit Assets
		wp_enqueue_script("startup-kit", get_template_directory_uri().$commonjs_path."/startup-kit.js", array("jquery", "bootstrap", "modernizr", "page-transitions", "easing-min"), $cache_ver, true);
		// Twisted Tools Main Scripts
		wp_enqueue_script("application", get_template_directory_uri().$js_dir."/application.js", array("jquery", "bootstrap", "modernizr", "page-transitions", "startup-kit"), $cache_ver, true);
		// Conditionally included scripts
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
		wp_enqueue_script("nivo-slider", get_template_directory_uri().$js_dir."/nivo-lightbox.min.js", array("application"), $cache_ver, true);
	}
endif;
add_action( 'wp_enqueue_scripts', 'load_scripts' );

if ( ! function_exists( 'load_script_fixes_and_overrides' ) ) :
	// Gurantee last executation priority
	function load_script_fixes_and_overrides() {
		global $cache_ver;
		// Twisted Tools Fixes & Overrides (to be loaded last always)
		wp_enqueue_script("fixes-overrides", get_template_directory_uri() . '/js/fixes-overrides.js', array("application"), $cache_ver, true);

		do_action('trigger_deferred_scripts');
	}
endif;
add_action( 'wp_enqueue_scripts', 'load_script_fixes_and_overrides', '99999999999' );

if ( ! function_exists( 'load_shortcut_icon' ) ) :
	// Loads Favicon and apple icon@2x
	function load_shortcut_icon() {
		?>
			<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri();?>/favicon.ico" />
	    <link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri();?>/apple-touch-icon.png" />
		<?php 
	}
endif;
add_action('wp_head','load_shortcut_icon');

if ( ! function_exists( 'load_fonts' ) ) :
	// Load Theme Styles
	function load_fonts() {
		global $cache_ver;
		
		$css_path=get_template_directory_uri()."/css";
		$font_css_file="/fonts.css";

		wp_enqueue_style("fonts", $css_path.$font_css_file, array("application"), $cache_ver);
	}
endif;
add_action( 'queue_fonts', 'load_fonts' );


function load_audioplayer_styles() {
	global $cache_ver;
	wp_enqueue_style("audioplayer", "/wp-content/plugins/fullwidth-audio-player/css/jquery.fullwidthAudioPlayer.css", array("application"), $cache_ver);
	wp_enqueue_style("font-awesome", "http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css", array("application"), $cache_ver);
}
add_action( 'wp_enqueue_scripts', 'load_audioplayer_styles', 10000 );

function load_audioplayer_scripts() {
	?>
	<div id="fap"></div>
	<script src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
	<script src="/wp-content/plugins/fullwidth-audio-player/js/soundcloud.sdk.js"></script>
	<script src="/wp-content/plugins/fullwidth-audio-player/js/amplify.min.js"></script>
	<script src="/wp-content/plugins/fullwidth-audio-player/js/soundmanager2-nodebug-jsmin.js"></script>
	<script src="/wp-content/plugins/fullwidth-audio-player/js/jquery.fullwidthAudioPlayer.min.js"></script>
	<script type="text/javascript">
			soundManager.url = "http://vcbv-s5sy.accessdomain.com/wp-content/plugins/fullwidth-audio-player/swf/";
					soundManager.flashVersion = 9;
					soundManager.useHTML5Audio = true;
					soundManager.debugMode = false;

										jQuery(window).load(function(){
											$ = jQuery.noConflict();

						setTimeout(function() {

							jQuery('#fullwidthAudioPlayer').fullwidthAudioPlayer({
								opened: 0,
								volume: 0,
								playlist: 0,
								autoPlay: 0,
								autoLoad:1,
								playNextWhenFinished: 1,
								keyboard: 1,
								socials: 0,
								wrapperColor: '#f0f0f0',
								mainColor: '#3c3c3c',
								fillColor: '#e3e3e3',
								metaColor: '#666666',
								strokeColor: '#e0e0e0',
								fillColorHover: '0',
								activeTrackColor: '#E8E8E8',
								wrapperPosition: window.fapPopupWin ? 'popup' : 'bottom',
								mainPosition: 'center',
								height: 70,
								playlistHeight: 210,
								coverSize: [50,50],
								offset: 20,
								twitterText: 'Share on Twitter',
								facebookText: 'Share on Facebook',
								downloadText: '',
								popupUrl: 'http://vcbv-s5sy.accessdomain.com/wp-content/plugins/fullwidth-audio-player/popup.html',
								autoPopup: 0,
								randomize: 0,
								shuffle:0,
								base64: 0,
								sortable: 0,
								hideOnMobile: 1,
								loopPlaylist : 0,
								storePlaylist: 0,
								layout: 'fullwidth',
								keepClosedOnceClosed: 1,
								animatePageOnPlayerTop: 0,
								openPlayerOnTrackPlay: 1,
								popup: 1,
								openLabel: '+',
								closeLabel: '×'
							});
						}, 201);

						jQuery('.fap-popup-player').click(function() {
							jQuery.fullwidthAudioPlayer.popUp();
							return false;
						});

						jQuery('.fwap-clear-button').on('click', function() {
							jQuery.fullwidthAudioPlayer.clear();
							return false;
						});

						var playText = "Play",
							pauseText = "Pause",
							currentTrackUrl = null,
							selectedPlayButton = null;
						jQuery('#fullwidthAudioPlayer').on('onFapTrackSelect', function(evt, trackData, playState) {

							if(trackData.duration == null) {
								//mp3,official.fm
								currentTrackUrl = trackData.stream_url;

								if(currentTrackUrl.search('official.fm') != -1) {

									currentTrackUrl = trackData.permalink_url;

								}
							}
							else {
								//soundcloud
								currentTrackUrl = trackData.permalink_url;

							}

							//pre-selected track button to play
							if(selectedPlayButton != null) {
								selectedPlayButton.text(playText);
							}
							//search for a play button with the same title
							if( 0 ) {
								currentTrackUrl = Base64.encode(currentTrackUrl);
							}
							else {
								currentTrackUrl = currentTrackUrl.replace(/.*?:\/\//g, "").replace(/^www./, "");
							}

							selectedPlayButton = jQuery('.fap-play-button[href*="'+currentTrackUrl+'"]');
							if(playState) {
								selectedPlayButton.text(pauseText);
							}
							else {
								selectedPlayButton.text(playText);
							}
						})
						.on('onFapPlay', function() {
							if(selectedPlayButton != null) {
								selectedPlayButton.text(pauseText);
							}
						})
						.on('onFapPause', function() {
							if(selectedPlayButton != null) {
								selectedPlayButton.text(playText);
							}
						});

						var trackWrapperMargin = null;

						jQuery(window).resize(function() {

							jQuery('.fap-center').each(function(i, item) {

								var $item = $(item),
									$trackWrapper = $item.children('div:first'),
									trackWrapperWidth = $trackWrapper.outerWidth(true),
									numOfTracksPerRow = Math.floor($item.parent().width() / trackWrapperWidth);

								if(trackWrapperMargin === null) {
									trackWrapperMargin = parseInt($trackWrapper.css('marginRight'));
								}

								$item.children('div').css('marginRight', trackWrapperMargin)
								.filter(':nth-child('+numOfTracksPerRow+'n)').css('marginRight', 0);

								if(numOfTracksPerRow == 1) {
									$item.width(trackWrapperWidth);
								}
								else {
									$item.width((trackWrapperWidth * numOfTracksPerRow) - trackWrapperMargin);
								}
							});
						}).resize();
					});		
				</script>
				<?php
}
add_action( 'wp_footer', 'load_audioplayer_scripts', 100000 );

