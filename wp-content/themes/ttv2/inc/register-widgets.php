<?php
/**
 * Widget Registration
 *
 * @package Twisted Tools v2
 */

/**
 * Register widget area sidebar-1.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function ttv2_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Sidebar', 'ttv2' ),
    'id'            => 'sidebar-default',
    'description'   => 'Default Sidebar',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );
}
add_action( 'widgets_init', 'ttv2_widgets_init' );