<?php
/*
 * Custom template functions for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Twisted Tools v2
 */

function product_content_override() {
	global $post, $product_meta, $sections;
	$product_post = get_post();
	$product_fields = get_post_custom();
	$product_meta = build_product_meta();
	$query_args = array(
		'post_type' => 'product_section',
		'numberposts' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);
	$sections = types_child_posts("product_section");
	$output = '';
	//die_with_message(print_r($post));
	for ($i=0; $i<=(sizeof($sections)-1); $i++) {
		$output .= render_section($sections[$i])."\n";
	} 
	$output = '<div id="product-sections">'."\n".$output.'</div>'."\n";
	return $output;
}

function render_section($section_post) {
	global $product_meta ,$section_meta;
		build_section_meta($section_post);
		$template_post = get_post(wpcf_pr_post_get_belongs($section_post->ID,'section_template'));
		$template = $template_post->post_content;
		$output = parse_mustache_template($template);
		return $output;
}

function render_section_menu() {
	return "<!-- insert javascript -->";
}

function build_section_meta($section_post) {
	global $post, $section_meta;
	$section_id = $section_post->ID;
	$section_fields = get_post_custom($section_id);
	die_with_message(print_r($section_fields));
	$section_post = get_post($section_id);
	$section_type = $section_fields['wpcf-sectiontype'][0];
	
	switch ($section_type) {
	  case 'header1':
	  	if (isset($section_fields['wpcf-'.'headingimage'])) {
	  		$image_url = $section_fields['wpcf-'.'headingimage'][0];
	  	} else {
	  		$image_url = null;
	  	}
	    $section_meta = [
	    	"id" => $section_id,
	  		"image" => $image_url		
			];
	  break;
	  case 'feature1':
	  if (isset($section_fields['wpcf-'.'feature1'][0])) {
	  	$feature1 = $section_fields['wpcf-'.'feature1'];
	  } else {
	  	$feature1 = null;
	  }
	  if (isset($section_fields['wpcf-'.'feature2'][0])) {
	  	$feature2 = $section_fields['wpcf-'.'feature2'];
	  } else {
	  	$feature2 = null;
	  }
	  if (isset($section_fields['wpcf-'.'feature3'][0])) {
	  	$feature3 = $section_fields['wpcf-'.'feature3'];
	  } else {
	  	$feature3 = null;
	  }
	  if ($section_fields['wpcf-includeprimarymediafeature'] == 1) {
	  	$primary_media_feature = true;
	  	$primary_media_type = $section_fields['wpcf-primarymediafeaturetype'];
	  	$primary_media_text = $section_fields['wpcf-primarymediafeaturetext'];
	  	$primary_media_url = $section_fields['wpcf-primarymediafeatureurl'];
	  } else {
	  	$primary_media_feature = false;
	  	$primary_media_type = null;
	  	$primary_media_text = null;
	  	$primary_media_url = null;
	  }
	  if ($section_fields['wpcf-includesecondarymediafeature'] == 1) {
	  	$secondary_media_feature = true;
	  	$secondary_media_type = $section_fields['wpcf-secondarymediafeaturetype'];
	  	$secondary_media_text = $section_fields['wpcf-secondarymediafeaturetext'];
	  	$secondary_media_url = $section_fields['wpcf-secondarymediafeatureurl'];
	  } else {
	  	$secondary_media_feature = false;
	  	$secondary_media_type = null;
	  	$secondary_media_text = null;
	  	$secondary_media_url = null;
	  }
	  if (isset($section_fields['wpcf-'.'primaryimage'])) {
	  		$image_url = $section_fields['wpcf-'.'primaryimage'][0];
	  	} else {
	  		$image_url = null;
	  	}
  	$section_meta = [
    	"id" => $section_id,
  		"tagline" => $section_fields['wpcf-'.'featuretagline'],
  		"image" => $image_url,
  		"feature1" => $feature1,
  		"feature2" => $feature2,
  		"feature3" => $feature3,
  		"primary_media" => $primary_media_feature,
  		"primary_media_type" => $primary_media_type,
  		"primary_media_text" => $primary_media_text,
  		"primary_media_url" => $primary_media_url,
  		"secondary_media" => $secondary_media_feature,
  		"secondary_media_type" => $secondary_media_type,
  		"secondary_media_text" => $secondary_media_text,
  		"secondary_media_url" => $secondary_media_url
		];
  break;
	}
	/*$section_meta = [
  	"title" => $product_post->post_title,
  	"regular_price" => $product_fields['_regular_price'][0],
  	"sale_price" => $product_fields['_sale_price'][0]
	];*/
	//die_with_message(print_r($section_fields));
	return $section_meta;
}

function build_product_meta() {
	global $post, $product_meta;
	//$section_fields = get_post_custom();
	//$product_id = wpcf_pr_post_get_belongs(get_the_ID(),'product');
	$product_id = $post->ID;
	$product_fields = get_post_custom($product_id);
	$product_post = get_post($product_id);
	$product_meta = [
  	"title" => $product_post->post_title,
  	"regular_price" => $product_fields['_regular_price'][0],
  	"sale_price" => $product_fields['_sale_price'][0]
	];
	return $product_meta;
}


function get_product_data($key) {
	global $product_meta, $section_meta, $section_meta;

	if ($key) {
		$key = str_replace(array( '{', '}' ), '', $key);
		switch ($key) {
			case 'header_brand':
		    return build_header_brand();
		  break;
		  case 'product_name':
		    return $product_meta['title'];
		  break;
		  case 'product_menu':
		    return '';
		  break;
		  case 'product_price':
		    return $product_meta['regular_price'];
		  break;
		  case 'sale_price':
		    return $product_meta['sale_price'];
		  break;  
		  case 'header_image':
		    return $section_meta['image'];
		  break;  

		  case 'feature_image':
		  	return build_feature_image();
		  break;
		  case 'feature_tagline':
		    return $section_meta['image'];
		  break;
		  case 'feature_list':
		    return build_feature_list();
		  break;
		  case 'feature_primary_media_button':
		    return build_primary_media_button();
		  break;
		  case 'feature_secondary_media_button':
		    return build_secondary_media_button();
		  break;
		}
	}
}
function build_feature_image() {
	global $section_meta, $product_meta;
	if ($section_meta['image']) {
		$alt_text = $product_meta['title'];
		return '<img src="'.$section_meta['image'].'" alt="'.$alt_text.'">';
	}
}

function build_primary_media_button() {
	global $section_meta, $product_meta;
	if ($section_meta['primary_media'] == true) {
		$button_html - build_media_button($section_meta['primary_media_type'], $section_meta['primary_media_text'], $section_meta['primary_media_url']);
		return $button_html;
	}
}

function build_secondary_media_button() {
	global $section_meta, $product_meta;
	if ($section_meta['secondary_media'] == true) {
		$button_html - build_media_button($section_meta['secondary_media_type'], $section_meta['secondary_media_text'], $section_meta['secondary_media_url']);
		return $button_html;
	}
}

function build_media_button($type, $text, $url) {
	if ($type == 'audio') {
		$output = '<a class="btn btn-inverse fap-single-track" href="'.$url.'"><i class="icon-music"></i>'.$text.'</a>';
	}else if ($type == 'video') {
		$output = '<a class="btn btn-inverse video-lightbox" href="'.$url.'"><i class="icon-film"></i>'.$text.'</a>';
	}
	return $output;
}

function build_feature_list() {
	global $section_meta, $product_meta;
	$output = '<ul class="feature-list">'."\n";
	if ($section_meta['feature1'])
		$output .= "<li>".$section_meta['feature1']."</li>";
	if ($section_meta['feature2'])
		$output .= "<li>".$section_meta['feature2']."</li>";
	if ($section_meta['feature3'])
		$output .= "<li>".$section_meta['feature3']."</li>";
	$output .= "</ul>";
	return $output;
}

function build_header_brand() {
	global $section_meta, $product_meta;
	if ($section_meta['image']) {
		$alt_text = $product_meta['title'];
		return '<img src="'.$section_meta['image'].'" alt="'.$alt_text.'">';
	} else {
		return "<h3>".$product_meta['title']."</h3>";
	}
}

function parse_mustache_template($template_string) {
	if (preg_match_all('/{([^{|}]*)}/', $template_string, $output, PREG_PATTERN_ORDER)) {
		$matches = $output[0];
		$template = $template_string;
		for ($i=0; $i<=(sizeof($matches)-1); $i++) {
			$replacement_content = get_product_data($matches[$i]);
			$template_tag = "{".$matches[$i]."}";
			$template = preg_replace($template_tag, $replacement_content, $template);
		} 
		return $template;
	}
}
function die_with_message($message) {
	return wp_die( '<pre>' . $message . '</pre>' );
}

/*function render_page($content) {
	if (is_page()) {
		$query_args = array(
			'post_type' => 'page_section',
			'numberposts' => -1,
			'orderby' => 'menu_order',
			'order' => 'ASC'
		);
		$sections = types_child_posts("page_section", $query_args);
		die_with_message(print_r($sections));
		if (sizeof($sections) > 0) {
			return render_page_sections($sections);
		} else {
			return render_page_content();
		}
	} else {
		return $content;
	}
}
add_filter('the_content', 'render_page', 1);

function render_page_sections($sections) {
	
	$output = '';
	for ($i=0; $i<=(sizeof($sections)-1); $i++) {
		$section = get_post($sections[$i]->ID);
		$section_body = $section->post_content;
		$output .= $section_body."\n";
	}
	return $output;
} 
function render_page_content() {
	die_with_message('render_page_content');
	//die_with_message($);
	$page_id = get_query_var('page_id');
	$page_data = get_page($page_id);
	return $page_data->post_content;
} */
