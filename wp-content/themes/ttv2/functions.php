<?php
/**
 * Twisted Tools v2 functions and definitions
 *
 * @package Twisted Tools v2
 */

// Global Variables

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

// Theme functions

/**
 * Theme Init and Setup (styles, scripts, icons, etc)
 */
require get_template_directory() . '/inc/theme-setup.php';

/**
 * Register Widgets
 */
require get_template_directory() . '/inc/register-widgets.php';

/**
 * Asset loading (styles, scripts, icons, etc)
 */
require get_template_directory() . '/inc/asset-loading.php';

// Base Functions

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load shortcodes.
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * Enable/Load Post Formats
 */
$post_formats_list = array( 'aside', 'image', 'quote', 'video', 'audio', 'gallery' );
add_theme_support( 'post-formats', $post_formats_list );


/**
 * Register Nav Menus
 */
register_nav_menus( array(
    'topnav' => __( 'Topnav Menu', 'ttdev' ),
) );

// Stop Wordpress auto <p> and <br> tags in posts and pages

function remove_autop( $content ) {
  //'page_section' === get_post_type() && remove_filter( 'the_content', 'wpautop' );
  remove_filter( 'the_content', 'wpautop' );
  return $content;
}
add_filter( 'the_content', 'remove_autop', 0 );


/**
 * Load shortcodes.
 */
require get_template_directory() . '/inc/template-functions.php';

// THIRD PARTY FUNCTIONS

/**
 * wp-bootstrap-navwalker
 */
require get_template_directory()."/inc/third-party/wp_bootstrap_navwalker.php";

/**
 * global-hide-admin-toolbar-dev
 */
require get_template_directory()."/inc/third-party/global-hide-admin-toolbar-dev.php";

// Clean up <head>
function clean_head() {
  //category feeds
  remove_action( 'wp_head', 'feed_links_extra', 3 ); 
  //post and comments feed
  remove_action( 'wp_head', 'feed_links', 2 ); 
  //only required if you are looking to blog using an external tool
  remove_action( 'wp_head', 'rsd_link'); 
  //something to do with windows live writer
  remove_action( 'wp_head', 'wlwmanifest_link');
  //next previous post links
  remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0); 
  //generator tag
  remove_action( 'wp_head', 'wp_generator'); 
  //short links like ?p=124
  remove_action( 'wp_head', 'wp_shortlink_wp_head'); 
}
clean_head();
 
function clean_comment_form( $defaults ) {
	//remove html code hints below comment message form
	$defaults[ 'comment_notes_after' ] = '';
	//remove "website" field
	$defaults[ 'fields' ][ 'url' ] = '';
	return $defaults;
}
add_filter( 'comment_form_defaults', 'clean_comment_form' );