<section class="content-24 bg-midnight-blue" style="background-color:#1F2225;">
  <div class="image ultraloop"></div>
  <img class="ultraloop image" src="/wp-content/themes/ttv2/img/ultraloop.jpg" alt="" />
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-6">
        <div class="features features-clear">
          <div class="features-header">
            <div class="box">
              <div class="fui-cmd"></div>
            </div>
            <div class="box">
              <div class="fui-mic"></div>
            </div>
            <div class="box active">
              <div class="fui-user"></div>
            </div>
          </div>
          <div class="features-bodies" style="height: 254px;">
            <div class="features-body" style="height: 254px;">
              <h3>Multi useful components</h3>
              <p>Startup Design Framework contains components and complex blocks which can easily be integrated into almost any design. All of these components are made in the same style.
              </p>
              <a class="btn btn-clear" href="#">TRY IT NOW</a>
            </div>
            <div class="features-body" style="height: 254px;">
              <h3>Features of Startup Framework</h3>
              <p>We’ve created the product that will help your startup to look even better
              </p>
              <a class="btn btn-clear" href="#">TRY IT NOW</a>
            </div>
            <div class="features-body active" style="height: 254px;">
              <h3>Multi useful components</h3>
              <p>Startup Design Framework contains components and complex blocks which can easily be integrated into almost any design. All of these components are made in the same style.
              </p>
              <a class="btn btn-clear" href="#">TRY IT NOW</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php /*
<section class="header-17-sub text-center bg-midnight-blue">
  <div class="background">&nbsp;</div>
  <div class="container">

    <div class="row">
      <div class="col-sm-10 col-sm-offset-1">
        <div class="hero-unit">
          <h1>Features of Startup Framework</h1>
          <p>We’ve created the product that will help your startup to look even better</p>
        </div>
        <a class="btn btn-huge btn-clear" href="#"><span class="fui-facebook"> </span> Sign In with Facebook</a>
      </div>
    </div>

    <?php get_template_part('partials/home', 'mainslider'); ?>

  </div>
</section>
*/