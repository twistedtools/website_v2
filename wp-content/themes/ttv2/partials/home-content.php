            <section class="content-1">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10">
                            <h3>Multi useful components</h3>
                            <p>
                                Startup Design Framework contains components and complex blocks which can easily be integrated into
                                almost any design.
                                All of these components are made in the same style, and can easily be integrated into projects.
                            </p>

                            <div class="row box-video">

                                <div class="col-sm-7">
                                    <div class="player">
                                        <iframe id="pPlayer" src="http://player.vimeo.com/video/80282064?title=0&amp;byline=0&amp;portrait=0&amp;api=1&amp;player_id=pPlayer" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                    </div>
                                    <h6>Introduction</h6>
                                    <p>To get started, you select the any sample and base the entire website on it.</p>
                                </div>
                                <div class="col-sm-4 col-sm-offset-1">
                                    <div class="btns">
                                        <a class="btn btn-inverse" href="#"><span class="fui-apple"> </span>Buy in Appstore</a>
                                        <a class="btn btn-primary" href="#"><span class="fui-android"> </span>Buy in Google Play</a>
                                    </div>
                                    <div class="additional-links">
                                        Be sure to take a look to our
                                        <br class="hidden-xs">
                                        <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- content-5  -->
            <section class="content-5">
                <div class="container">
                    <img src="/wp-content/themes/ttv2/img/magic-wand@2x.png" width="200" height="200" alt="">
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1">
                            <h3>Perfect for your project</h3>
                            <p class="lead">
                                You have the design, you have the code
                            </p>
                        </div>
                    </div>
                    <div class="row features">
                        <div class="col-sm-4 col-sm-offset-2">
                            <span class="fui-cmd"> </span>
                            <h6>Save Your Time</h6>
                            <p>
                                Use your time for generating new ideas for your startup.
                                Take a break from the routine and work for new ideas.
                            </p>
                        </div>
                        <div class="col-sm-4 col-sm-offset-1">
                            <span class="fui-windows"> </span>
                            <h6>Responsive Layout</h6>
                            <p>
                                Of course we haven’t forgotten about the responsive layout. Create a website with full mobile support.
                            </p>
                        </div>
                    </div>
                    <div class="row features">
                        <div class="col-sm-4 col-sm-offset-2">
                            <span class="fui-upload"> </span>
                            <h6>Retina Ready</h6>
                            <p>
                                Startup Framework works fine on devices supporting Retina Display. Feel the clarity in each pixel.
                            </p>
                        </div>
                        <div class="col-sm-4 col-sm-offset-1">
                            <span class="fui-credit-card"> </span>
                            <h6>Money Economy</h6>
                            <p>
                                Startups can save money on design and code and spend it on concept, ideas and functionality.
                            </p>
                        </div>
                    </div>
                </div>
            </section>