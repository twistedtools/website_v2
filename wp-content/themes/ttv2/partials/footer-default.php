<footer class="footer-1 bg-midnight-blue" role="contentinfo">
  <div class="container">
    <div class="row">
      <div class="col-sm-5">
        <p class="lead">
          <b>200,000</b> users registered since January
        </p>
        <div class="social-btns">
          <a href="#" class="social-btn-facebook sharrre" data-text="Startup Design Framework - http://designmodo.com/startup/ Suit Up your Startup!" data-url="http://designmodo.com/startup/">
            <div class="fui-facebook"></div>
            <div class="fui-facebook"></div>
          <div class="buttons"><div class="button facebook"><div id="fb-root"></div><div class="fb-like" data-href="http://designmodo.com/startup/" data-send="false" data-layout="button_count" data-width="" data-show-faces="false" data-action="like" data-colorscheme="" data-font="" data-via="undefined"></div></div></div></a>
          <a href="#" class="social-btn-twitter sharrre" data-text="Startup Design Framework - http://designmodo.com/startup/ Suit Up your Startup!" data-url="http://designmodo.com/startup/">
            <div class="fui-twitter"></div>
            <div class="fui-twitter"></div>
          <div class="buttons"><div class="button twitter"><a href="https://twitter.com/share" class="twitter-share-button" data-url="false" data-count="horizontal" data-text="Startup Design Framework - http://designmodo.com/startup/ Suit Up your Startup!" data-via="Designmodo" data-hashtags="" data-related="" data-lang="en">Tweet</a></div></div></a>
        </div>
      </div>
      <nav>
        <div class="col-sm-2 col-sm-offset-1">
          <h6>About</h6>
          <ul>
            <li class="active">
              <a href="#">About US</a>
            </li>
            <li>
              <a href="#">Blog</a>
            </li>
            <li>
              <a href="#">Team</a>
            </li>
            <li>
              <a href="#">Career</a>
            </li>
            <li>
              <a href="#">Contact</a>
            </li>
          </ul>
        </div>
        <div class="col-sm-2">
          <h6>Follow Us</h6>
          <ul>
            <li>
              <a href="#">Facebook</a>
            </li>
            <li>
              <a href="#">Twitter</a>
            </li>
            <li>
              <a href="#">Instagram</a>
            </li>
          </ul>
        </div>
      </nav>
      <div class="col-sm-2 buy-btn">
        <a class="btn btn-danger btn-block" href="#">Buy App</a>
        or <a href="#">Learn More</a>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-5 additional-links">
        <a href="#">Terms of Service</a>
        <a href="#">Special Terms</a>
        <a href="#">Privacy Policy</a>
      </div>
    </div>
  </div>
</footer>