<div id="navwrap" class="container-fluid bg-midnight-blue">
  <nav id="topnav" class="navbar col-sm-12" role="navigation">

    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-topnav-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="brand" href="<?php bloginfo( 'url' ); ?>">
        <div class="image logo">&nbsp;</div>
      </a>
    </div>
    <div id="bs-topnav-navbar-collapse" class="collapse navbar-collapse">
      <?php wp_nav_menu( array(
        'menu'              => 'topnav',
        'theme_location'    => 'topnav',
        'depth'             => 2,
        'container'         => false,
        'menu_class'        => 'nav pull-right',
        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
        'walker'            => new wp_bootstrap_navwalker())
      ); ?>
    </div>
  
  </nav>
</div>