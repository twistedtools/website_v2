<?php
/**
 *  Quick Checkout Shortcodes
 *
 * @description: Add the shortcode capabilities of the plugin
 * @since      : 1.0
 * @created    : 2/7/14
 */

class Quick_Checkout_Shortcodes {

	protected $atts;

	public function __construct() {

		//Single Product Quick Checkout Shortcode
		add_shortcode( 'product_quick_checkout', array( $this, 'shortcode_product' ) );
		add_shortcode( 'open_checkout', array( $this, 'shortcode_open_checkout' ) );
		add_shortcode( 'reveal_quick_checkout', array( $this, 'shortcode_reveal_quick_checkout' ) );
		add_shortcode( 'show_quick_checkout', array( $this, 'shortcode_show_quick_checkout' ) );

		add_filter( 'body_class', array( $this, 'quick_cart_body_class' ) );

	}


	/**
	 * Quick Checkout Single Product Shortcode [product_quick_checkout id=""]
	 *
	 * @param $atts
	 *
	 * @return mixed
	 */
	function shortcode_product( $atts ) {

		global $woocommerce;

		//get the args for this shortcode
		extract(
			shortcode_atts(
				array(
					'checkout_text'         => __( 'Buy Now', 'wqc' ),
					'checkout_action'       => 'lightbox',
					'shop_cart_reveal'      => 'yes',
					'shop_cart_reveal_text' => __( 'Checkout Now', 'wqc' ),
					'clear_cart'            => 'false'
				), $atts, 'product_quick_checkout'
			)
		);

		//defaults
		$atts['checkout_text']         = isset( $atts['checkout_text'] ) ? $atts['checkout_text'] : $checkout_text;
		$atts['checkout_action']       = isset( $atts['checkout_action'] ) ? $atts['checkout_action'] : $checkout_action;
		$atts['clear_cart']            = isset( $atts['clear_cart'] ) ? $atts['clear_cart'] : $clear_cart;
		$atts['shop_cart_reveal']      = isset( $atts['shop_cart_reveal'] ) ? $atts['shop_cart_reveal'] : $shop_cart_reveal;
		$atts['shop_cart_reveal_text'] = isset( $atts['shop_cart_reveal_text'] ) ? $atts['shop_cart_reveal_text'] : $shop_cart_reveal_text;
		$atts['image_overlay']         = true;

		//Add params to AJAX for Shortcode Usage
		//http://benjaminrojas.net/using-wp_localize_script-dynamically/
		global $wp_scripts;
		$localized_data = array(
			'shortcode_shop_cart_reveal'      => $atts['shop_cart_reveal'],
			'shortcode_shop_cart_reveal_text' => $atts['shop_cart_reveal_text'],
			'shortcode_checkout_action'       => $atts['checkout_action'],
		);
		$data           = $wp_scripts->get_data( 'quick_checkout_scripts', 'data' );

		$this->array_push_localized_script( $data, $localized_data );


		//get product output
		$output = WC_Shortcodes::product( $atts );

		$qc_shop_options = get_option( 'woocommerce_quick_checkout_shop' );

		//output button only if shop option is disabled
		//otherwise, the button will display as expected
		if ( $qc_shop_options === 'no' ) {
			//get image hover button
			$button = $this->shortcode_button( $atts );
			//string replace with button inserted (filter provided for users with modified templates)
			$string_to_replace = apply_filters( 'shortcode_product_str_replace', '</li>' );

			//@TODO is there a better way to be doing this? Hooking runs for every product on page...
			$output = str_replace( $string_to_replace, $button . $string_to_replace, $output );
		}


		//Get checkout
		ob_start();
		WC_Quick_Checkout()->quick_checkout_get_checkout();
		$output .= ob_get_clean();

		//Return shortcode output
		return $output;

	}


	/**
	 * Quick Checkout Single Product Shortcode [reveal_quick_checkout id=""]
	 *
	 * @param $atts
	 *
	 * @return mixed
	 */
	function shortcode_reveal_quick_checkout( $atts ) {

		global $woocommerce;

		//get the args for this shortcode
		extract(
			shortcode_atts(
				array(
					'checkout_text'   => __( 'Buy Now', 'wqc' ),
					'checkout_action' => 'lightbox',
					'id'              => '',
					'variation_id'    => '',
					'quantity'        => '1',
					'clear_cart'      => 'true'
				), $atts, 'reveal_quick_checkout'
			)
		);

		//defaults
		$atts['checkout_text']   = isset( $atts['checkout_text'] ) ? $atts['checkout_text'] : $checkout_text;
		$atts['checkout_action'] = isset( $atts['checkout_action'] ) ? $atts['checkout_action'] : $checkout_action;
		$atts['clear_cart']      = isset( $atts['clear_cart'] ) ? $atts['clear_cart'] : $clear_cart;

		//Add params to AJAX for Shortcode Usage
		//http://benjaminrojas.net/using-wp_localize_script-dynamically/
		global $wp_scripts;
		$localized_data = array(
			'shortcode_checkout_action' => $atts['checkout_action'],
			'product_id'                => $id,
			'product_variation_id'      => $variation_id,
		);
		$data           = $wp_scripts->get_data( 'quick_checkout_scripts', 'data' );
		$this->array_push_localized_script( $data, $localized_data );

		//get product add to cart output
		$output = $this->shortcode_button( $atts );

		//Get checkout
		ob_start();
		WC_Quick_Checkout()->quick_checkout_get_checkout();
		$output .= ob_get_clean();

		//Return shortcode output
		return $output;

	}

	/**
	 * Quick Checkout Single Product Shortcode [show_quick_checkout id=""]
	 *
	 * @param $atts
	 *
	 * @return mixed
	 */
	function shortcode_show_quick_checkout( $atts ) {

		global $woocommerce;
		//get the args for this shortcode
		extract(
			shortcode_atts(
				array(
					'id'           => '',
					'quantity'     => '1',
					'variation_id' => '',
					'clear_cart'   => 'true',
				), $atts, 'show_quick_checkout'
			)
		);

		//Add Product to Cart
		$product_data = get_product( $id );

		//Add quantity to object for AJAX
		$product_data->quantity = $quantity;

		//Add params to AJAX for Shortcode Usage
		//http://benjaminrojas.net/using-wp_localize_script-dynamically/
		global $wp_scripts;
		$localized_data = array(
			'auto_add_to_cart'     => 'true',
			'product_id'           => $id,
			'product_args'         => $product_data,
			'product_variation_id' => $variation_id,
			'product_quantity'     => $quantity,
			'product_type'         => $product_data->product_type,
			'clear_cart'           => $clear_cart
		);
		$data           = $wp_scripts->get_data( 'quick_checkout_scripts', 'data' );
		$this->array_push_localized_script( $data, $localized_data );

		add_filter( 'quick_checkout_opening_tag', array( $this, 'revealed_open_tag' ), 10 );

		ob_start();
		WC_Quick_Checkout()->quick_checkout_get_checkout();
		$output = ob_get_clean();

		//Return shortcode output
		return $output;

	}


	/**
	 * Add WC classes to body
	 *
	 * Used for styling checkout used with shortcode
	 *
	 * @param $classes
	 *
	 * @return array
	 */
	function quick_cart_body_class( $classes ) {

		if ( ! in_array( 'woocommerce', $classes ) ) {
			$classes[] = 'woocommerce checkout';
		}

		return $classes;

	}

	/**
	 * Open Checkout Link
	 *
	 * Displays a link to open checkout from any page. Commonly placed in headers and sidebars for easier checkout.
	 *
	 * Shortcode [open_checkout]
	 */

	function shortcode_open_checkout( $atts ) {

		//get the args for this shortcode
		extract(
			shortcode_atts(
				array(
					'text' => __( 'Checkout Now', 'wqc' ),
				), $atts, 'open_checkout'
			)
		);

		$output = '<a href="#open-checkout" rel="nofollow" class="button quick-checkout-link">' . $text . '</a>';

		ob_start();
		WC_Quick_Checkout()->quick_checkout_get_checkout();
		$output .= ob_get_clean();

		//Return shortcode output
		return $output;


	}


	/**
	 * Shortcode Image Hover Button
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public function shortcode_button( $atts ) {

		$product = get_product( $atts['id'] );

		$variation_id = ! empty( $atts['variation_id'] ) ? $atts['variation_id'] : '';
		$quantity = ! empty( $atts['quantity'] ) ? $atts['quantity'] : '1';

		$shortcode_classes = apply_filters( 'shortcode_class', 'quick-checkout-button quick-checkout-button-shortcode' );

		if ( isset( $atts['image_overlay'] ) && $atts['image_overlay'] == true ) {
			$shortcode_classes .= ' quick-checkout-button-image-overlay';
		} else {
			$shortcode_classes .= ' button';
		}

		return '<a href="#quick-checkout" class="' . $shortcode_classes . '" data-product_id="' . esc_attr( $product->id ) . '"  data-variation_id="' . $variation_id . '" data-product_sku="' . esc_attr( $product->get_sku() ) . '"  data-quantity="' . esc_attr( $quantity ) . '" data-checkout-action="' . $atts['checkout_action'] . '" data-clear-cart="' . $atts['clear_cart'] . '">' . $atts['checkout_text'] . '</a>';

	}


	/**
	 *  Add params to AJAX for Shortcode Usage
	 *  http://benjaminrojas.net/using-wp_localize_script-dynamically/
	 *
	 */
	function array_push_localized_script( $data, $localized_data ) {

		global $wp_scripts;

		if ( empty( $data ) ) {
			wp_localize_script( 'quick_checkout_scripts', 'quick_checkout', $localized_data );
		} else {
			if ( ! is_array( $data ) ) {
				$data = json_decode( str_replace( 'var quick_checkout = ', '', substr( $data, 0, - 1 ) ), true );
			}
			foreach ( $data as $key => $value ) {
				$localized_data[$key] = $value;
			}
			$wp_scripts->add_data( 'quick_checkout_scripts', 'data', '' );
			wp_localize_script( 'quick_checkout_scripts', 'quick_checkout', $localized_data );
		}
	}


	/**
	 * Switch Quick Checkout opening tag with visible one
	 *
	 * Used for landing pages to display checkout with product already added to cart so it is visible by default
	 */

	function revealed_open_tag() {
		return '<div id="quick-checkout" style="display:block">';
	}


}