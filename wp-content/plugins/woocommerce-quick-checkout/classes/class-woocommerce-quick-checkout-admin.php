<?php
/**
 *  Quick Checkout Admin Class
 *
 * @description: Displays options in a custom WooCommerce tab
 * @since      :
 * @created    : 2/4/14
 */

class WC_Quick_Checkout_Admin {


	public function __construct() {

		add_action( 'admin_footer', array( $this, 'admin_footer' ), 999 );
		add_filter( 'woocommerce_settings_tabs_array', array( $this, 'quick_checkout_admin_tab_label' ), 30 );
		add_action( 'woocommerce_settings_tabs_quick_checkout', array( $this, 'admin_panel' ) );
		add_action( 'woocommerce_update_options_quick_checkout', array( $this, 'process_admin_options' ) );

		// Settings Link for Plugin page
		add_filter( 'plugin_action_links', array( $this, 'add_action_link' ), 9, 2 );

		//Settings Page for License
		add_action( 'admin_menu', array( $this, 'register_quick_checkout_license_submenu_page' ) );


		//Enqueue Scripts/Styles
		add_action( 'admin_enqueue_scripts', array( $this, 'quick_checkout_admin_enqueue' ) );

		//Include custom meta boxes and fields
		//https://github.com/WebDevStudios/Custom-Metaboxes-and-Fields-for-WordPress/wiki/Basic-Usage
		// Initialize the metabox class
		$this->single_product_enable_control = get_option( 'woocommerce_quick_checkout_product_metabox' );

		//Does user want control over individual products?
		if ( $this->single_product_enable_control == 'yes' ) {
			//Create metabox
			add_action( 'init', array( $this, 'quick_checkout_initialize_cmb_meta_boxes' ), 9999 );
			add_filter( 'cmb_meta_boxes', array( $this, 'quick_checkout_metaboxes' ) );

			//Enqueue Scripts/Styles
			add_action( 'admin_enqueue_scripts', array( $this, 'quick_checkout_metabox_admin_enqueue' ) );

		}

	}


	/**
	 * Add License Subpage
	 */
	function register_quick_checkout_license_submenu_page() {
		add_submenu_page(
			'options-general.php', //The parent page of this menu
			__( 'Quick Checkout', 'wqc' ), //The Menu Title
			__( 'Quick Checkout', 'wqc' ), //The Page Title
			'manage_options', // The capability required for access to this item
			'quick-checkout-license', //the slug to use for the page in the URL
			array( $this, 'quick_checkout_license_submenu_page_options' ) //The function to call to render the page
		);
	}

	/**
	 * Add the License Metabox
	 */
	function quick_checkout_license_submenu_page_options() {
		?>

		<div class="wrap">
			<h2 class="qc-icon"><?php _e('Quick Checkout License', 'wqc'); ?></h2>

			<p><?php echo sprintf( __( 'This settings page is where you manage your license for Quick Checkout. If you are looking for the plugin options page <a href="%s">click here</a> to be taken to that page.', 'wqc' ), 'admin.php?page=wc-settings&tab=quick_checkout' ); ?></p>

			<?php
			//Output Licensing Fields
			if ( class_exists( 'Quick_Checkout_Licence' ) ) {
				WC_Quick_Checkout()->licence->edd_wordimpress_license_page();
			} ?>

		</div>

	<?php
	}


	/**
	 * Initialize the metabox class
	 */
	function quick_checkout_initialize_cmb_meta_boxes() {
		if ( ! class_exists( 'cmb_Meta_Box' ) ) {
			require_once( WQC_PLUGIN_PATH . '/includes/metaboxes/init.php' );

		}
	}

	/**
	 * Create metaboxes
	 */
	function quick_checkout_metaboxes() {

		$global_option = get_option( 'woocommerce_quick_checkout_product' );
		$prefix        = 'qc_'; // Prefix for all fields
		$fields        = array();


		//Is global option enabled
		if ( $global_option == 'yes' ) {

			//Display Info to user
			$fields[] = $fields + array(
						'name' => 'Global Options Enabled',
						'desc' => 'Quick Checkout global settings have been enabled on the plugin settings page. All options set within this metabox will override global settings.',
						'type' => 'title',
						'id'   => $prefix . 'global_info'
					);


		} //Individual Options

		//Display disable option
		$fields[] = $fields + array(
					'name' => 'Enable ',
					'desc' => 'Enable Quick Checkout for this product',
					'id'   => $prefix . 'enable_checkbox',
					'type' => 'checkbox'
				);

		//Display disable option
		$fields[] = $fields + array(
					'name' => 'Disable ',
					'desc' => 'Disable Quick Checkout for this product on all pages',
					'id'   => $prefix . 'disable_checkbox',
					'type' => 'checkbox'
				);

		$fields[] = $fields + array(
					'name' => 'Clear Cart ',
					'desc' => 'Clear user\'s cart before opening checkout',
					'id'   => $prefix . 'single_cart_action',
					'type' => 'checkbox'
				);
		$fields[] = $fields + array(
					'name'    => 'Image Hover ',
					'desc'    => 'Show Quick Checkout button on product image hover; only supports simple products',
					'id'      => $prefix . 'single_product_image_button',
					'type'    => 'radio_inline',
					'options' => array(
						array( 'name' => 'Yes', 'value' => 'yes' ),
						array( 'name' => 'No', 'value' => 'no' ),
					)
				);

		$fields[] = $fields + array(
					'name'    => 'Display Option',
					'desc'    => '',
					'id'      => $prefix . 'single_display_option',
					'type'    => 'select',
					'options' => array(
						array( 'name' => 'Select option to customize...', 'value' => 'default' ),
						array( 'name' => 'Add Quick Checkout button after the Add to Cart button', 'value' => 'after' ),
						array( 'name' => 'Add Quick Checkout button before the Add to Cart button', 'value' => 'before' ),
						array( 'name' => 'Replace the Add to Cart button with Quick Checkout button', 'value' => 'replace' )
					)
				);

		$fields[] = $fields + array(
					'name'    => 'Checkout Action',
					'desc'    => '',
					'id'      => $prefix . 'single_product_action',
					'type'    => 'select',
					'options' => array(
						array( 'name' => 'Select option to customize...', 'value' => 'default' ),
						array( 'name' => 'Open checkout in a responsive lightbox', 'value' => 'lightbox' ),
						array( 'name' => 'Reveal checkout within product post content', 'value' => 'reveal' ),
					)
				);

		$fields[] = $fields + array(
					'name'    => __( 'Checkout position', 'wqc' ),
					'desc'    => __( 'This controls where the checkout will be revealed once the user chooses to checkout using the quick cart button. This only applies to when the "Reveal checkout on product post" option is enabled.', 'wqc' ),
					'id'      => $prefix . 'single_product_checkout_display_position',
					'type'    => 'select',
					'options' => array(
						array( 'name' => __( 'After single product', 'wqc' ), 'value' => 'woocommerce_after_single_product' ),
						array( 'name' => __( 'Before single product', 'wqc' ), 'value' => 'woocommerce_before_single_product' ),
						array( 'name' => __( 'After single product summary', 'wqc' ), 'value' => 'woocommerce_after_single_product_summary' ),
						array( 'name' => __( 'Before single product summary', 'wqc' ), 'value' => 'woocommerce_before_single_product_summary' ),
						array( 'name' => __( 'After main content', 'wqc' ), 'value' => 'woocommerce_after_main_content' ),
						array( 'name' => __( 'Before main content', 'wqc' ), 'value' => 'woocommerce_before_main_content' ),
						array( 'name' => __( 'After add to cart form', 'wqc' ), 'value' => 'woocommerce_after_add_to_cart_form' ),
						array( 'name' => __( 'Within product summary', 'wqc' ), 'value' => 'woocommerce_single_product_summary' ),
					)
				);

		$fields[] = $fields + array(
					'name' => 'Button Text',
					'desc' => '',
					'std'  => 'Buy Now',
					'id'   => $prefix . 'single_product_button_text',
					'type' => 'text_medium'
				);

		//Create Metabox
		$meta_boxes['qc_metabox'] = array(
			'id'         => 'qc_metabox',
			'title'      => 'Quick Checkout Options',
			'pages'      => array( 'product' ), // post type
			'context'    => 'normal',
			'priority'   => 'default',
			'show_names' => true, // Show field names on the left
			'fields'     => $fields
		);

		return $meta_boxes;
	}


	/**
	 * Admin Single Product Metabox Scripts
	 */
	function quick_checkout_metabox_admin_enqueue() {
		global $post_type;

		$suffix = defined( 'WQC_DEBUG' ) && WQC_DEBUG ? '' : '.min';

		//Only load scripts on Product CPT as to not slow down WP admin
		if ( $post_type == 'product' ) {

			wp_enqueue_script( 'quick_checkout_admin_metabox_js', WQC_PLUGIN_URL . '/assets/js/quick-checkout-admin-product' . $suffix . '.js', array( 'jquery' ) );
			wp_enqueue_style( 'quick_checkout_admin_metabox_css', WQC_PLUGIN_URL . '/assets/css/quick-checkout-admin-product' . $suffix . '.css' );

		}

	}

	/**
	 * Admin WooCommerce Quick Checkout Options Screen Scripts
	 */
	function quick_checkout_admin_enqueue( $hook ) {

		$suffix = defined( 'WQC_DEBUG' ) && WQC_DEBUG ? '' : '.min';

		if ( $hook == 'woocommerce_page_woocommerce_settings' || $hook == 'woocommerce_page_wc-settings' ) {

			wp_enqueue_script( 'quick_checkout_admin_js', WQC_PLUGIN_URL . '/assets/js/quick-checkout-admin' . $suffix . '.js' );
			wp_enqueue_style( 'quick_checkout_admin_css', WQC_PLUGIN_URL . '/assets/css/quick-checkout-admin' . $suffix . '.css' );

		}

	}


	/**
	 * Helper Link
	 *
	 * Adds link to plugin listing page to settings
	 *
	 * @param $links
	 * @param $file
	 *
	 * @return mixed
	 */
	public function add_action_link( $links, $file ) {

		if ( $file == WQC_PLUGIN_BASE ) {

			$settings_link = '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=quick_checkout' ) . '" title="' . __( 'Go to the settings page', 'wqc' ) . '">' . __( 'Settings', 'wqc' ) . '</a>';

			array_unshift( $links, $settings_link );
		}

		return $links;
	}


	function admin_footer() {
		if ( ! isset( $_GET['tab'] ) || 'quick_checkout' != $_GET['tab'] ) {
			return;
		}
		?>
		<style>
			#woocommerce_extensions {
				display: none !important;
			}
		</style>
	<?php

	}


	public static function quick_checkout_admin_tab_label( $settings ) {
		$settings['quick_checkout'] = __( 'Quick Checkout', 'wqc' );

		return $settings;
	}


	public static function add_settings_fields() {
		global $woocommerce_settings;

		$woocommerce_settings['quick_checkout'] = apply_filters( 'woocommerce_quick_checkout_settings', array(


			array( 'name' => __( 'SSL Options', 'wqc' ),
						 'type' => 'title',
						 'desc' => __( 'Manage where SSL is turned on in your WooCommerce shop. In order to ensure secure on page transactions you must Force SSL for the entire WooCommerce shop.', 'wqc' ),
						 'id'   => 'woocommerce_quick_checkout_ssl_options' ),

			array(
				'title'    => __( 'Enable SSL', 'wqc' ),
				'desc'     => __( 'Force SSL for all WooCommerce', 'wqc' ),
				'desc_tip' => __( 'This option is required to enable credit card payment gateways.', 'wqc' ),
				'id'       => 'woocommerce_quick_checkout_shop_ssl',
				'type'     => 'checkbox',
			),

			array( 'type' => 'sectionend', 'id' => 'woocommerce_quick_checkout_ssl_options' ),


			array( 'name' => __( 'Shop Page Display Options', 'wqc' ),
						 'type' => 'title',
						 'desc' => __( 'Manage how Quick Checkout displays on your website\'s shop page. The shop page displays the entire catalog of products.', 'wqc' ),
						 'id'   => 'woocommerce_quick_checkout_display_options_shop' ),


			array(
				'title'           => __( 'Shop Page', 'wqc' ),
				'desc'            => __( 'Enable Quick Checkout on the WooCommerce shop page', 'wqc' ),
				'id'              => 'woocommerce_quick_checkout_shop',
				'default'         => 'no',
				'type'            => 'checkbox',
				'show_if_checked' => 'option',
				'desc_tip'        => __( 'Check to enable the Quick Checkout option for WooCommerce shop pages.', 'wqc' ),
				'checkboxgroup'   => 'start',
			),

			array(
				'desc'            => __( 'Clear user\'s cart before opening checkout', 'wqc' ),
				'id'              => 'woocommerce_quick_checkout_shop_cart_action',
				'default'         => 'no',
				'type'            => 'checkbox',
				'checkboxgroup'   => '',
				'show_if_checked' => 'yes',
			),

			array(
				'title'         => __( 'Quick Checkout Display', 'wqc' ),
				'desc'          => __( 'Display Quick Checkout button on product image hover', 'wqc' ),
				'id'            => 'woocommerce_quick_checkout_shop_image_hover',
				'default'       => 'yes',
				'type'          => 'checkbox',
				'checkboxgroup' => 'start'
			),

			array(
				'desc'          => __( 'Reveal Quick Checkout link after product added to cart', 'wqc' ),
				'id'            => 'woocommerce_quick_checkout_shop_cart_reveal',
				'default'       => 'yes',
				'type'          => 'checkbox',
				'checkboxgroup' => 'end'
			),

			array(
				'title'   => __( 'Hover Button Text', 'wqc' ),
				'desc'    => '',
				'id'      => 'woocommerce_quick_checkout_shop_cart_button_text',
				'default' => __( 'Buy Now', 'wqc' ),
				'type'    => 'text',
				'css'     => 'min-width:200px;',
			),
			array(
				'title'   => __( 'Reveal Link Text', 'wqc' ),
				'desc'    => '',
				'id'      => 'woocommerce_quick_checkout_shop_cart_link_text',
				'default' => __( 'Checkout Now', 'wqc' ),
				'type'    => 'text',
				'css'     => 'min-width:200px;',
			),

			array(
				'title'    => __( 'Shop Action', 'wqc' ),
				'id'       => 'woocommerce_quick_checkout_shop_action',
				'class'    => 'qc_shop_action',
				'default'  => 'lightbox',
				'type'     => 'select',
				'desc_tip' => __( 'This option is important as it will affect how Quick Checkout functions on the shop page.', 'wqc' ),
				'options'  => array(
					'lightbox' => __( 'Open checkout in a responsive lightbox', 'wqc' ),
					'reveal'   => __( 'Reveal checkout on shop page', 'wqc' )
				),
			),

			array(
				'title'    => __( 'Checkout Position', 'wqc' ),
				'desc'     => __( 'This controls where the checkout will be revealed once the user chooses to checkout using the Quick Checkout button. This only applies to when the "Reveal checkout on shop page" option is enabled.', 'wqc' ),
				'id'       => 'woocommerce_quick_checkout_checkout_display_position',
				'css'      => 'min-width:150px;',
				'default'  => 'woocommerce_before_shop_loop',
				'type'     => 'select',
				'options'  => array(
					'woocommerce_before_shop_loop' => __( 'Above shop loop', 'wqc' ),
					'woocommerce_after_shop_loop'  => __( 'Below shop loop', 'wqc' ),
				),
				'desc_tip' => true,
			),


			array( 'type' => 'sectionend', 'id' => 'woocommerce_quick_checkout_display_options_shop' ),


			array( 'name' => __( 'Individual Product Post Display Options', 'wqc' ),
						 'type' => 'title',
						 'desc' => __( 'Provides full control over Quick Checkout on single product posts. Options set on the individual product post level override global settings.', 'wqc' ),
						 'id'   => 'woocommerce_quick_checkout_display_options_product_single' ),

			array(
				'title'           => __( 'Single Products', 'wqc' ),
				'desc'            => __( 'Enable Quick Checkout Metabox for the WooCommerce Product Type', 'wqc' ),
				'id'              => 'woocommerce_quick_checkout_product_metabox',
				'default'         => 'no',
				'type'            => 'checkbox',
				'show_if_checked' => 'option',
				'desc_tip'        => sprintf( __( 'Check to enable the Quick Checkout metabox for WooCommerce <a href="%s">single product posts</a>.', 'wqc' ), 'edit.php?post_type=product' ),
				'checkboxgroup'   => 'start',
			),


			array( 'type' => 'sectionend', 'id' => 'woocommerce_quick_checkout_display_options_shop' ),


			array( 'name' => __( 'Global Product Post Display Options', 'wqc' ),
						 'type' => 'title',
						 'desc' => __( 'Manage how Quick Checkout displays globally for WooCommerce single product posts. Individual product Quick Checkout settings will override global settings.', 'wqc' ),
						 'id'   => 'woocommerce_quick_checkout_display_options_product_global' ),


			array(
				'title'           => __( 'All Products', 'wqc' ),
				'desc'            => __( 'Enable Quick Checkout globally for individual product posts', 'wqc' ),
				'id'              => 'woocommerce_quick_checkout_product',
				'default'         => 'no',
				'type'            => 'checkbox',
				'show_if_checked' => 'option',
				'desc_tip'        => __( 'Check to enable Quick Checkout on all WooCommerce single product posts.', 'wqc' ),
				'checkboxgroup'   => 'start',
			),


			array(
				'desc'            => __( 'Display Quick Checkout on Related Products', 'wqc' ),
				'id'              => 'woocommerce_quick_checkout_related_products',
				'default'         => 'no',
				'type'            => 'checkbox',
				'checkboxgroup'   => '',
				'show_if_checked' => 'yes',
			),

			array(
				'desc'            => __( 'Clear user\'s cart before opening checkout', 'wqc' ),
				'id'              => 'woocommerce_quick_checkout_product_cart_action',
				'default'         => 'no',
				'type'            => 'checkbox',
				'checkboxgroup'   => '',
				'show_if_checked' => 'yes',
			),

			array(
				'title'    => __( 'Quick Checkout Display', 'wqc' ),
				'id'       => 'woocommerce_quick_checkout_product_button_display',
				'class'    => 'qc_product_button_display',
				'default'  => 'after',
				'type'     => 'select',
				'desc_tip' => __( 'This option is important as it will affect how the quick cart functions on the shop page.', 'wqc' ),
				'options'  => array(
					'after'   => __( 'Add Quick Checkout button after the Add to Cart button ', 'wqc' ),
					'before'  => __( 'Add Quick Checkout button before the Add to Cart button ', 'wqc' ),
					'replace' => __( 'Replace the Add to Cart button with Quick Checkout button', 'wqc' ),
				),
			),

			array(
				'desc'    => __( 'Display Quick Checkout button on product image hover', 'wqc' ),
				'id'      => 'woocommerce_quick_checkout_product_image_button',
				'default' => 'no',
				'type'    => 'checkbox',
			),


			array(
				'title'    => __( 'Quick Checkout Action', 'wqc' ),
				'id'       => 'woocommerce_quick_checkout_product_action',
				'class'    => 'qc_product_action',
				'default'  => 'lightbox',
				'type'     => 'select',
				'desc_tip' => __( 'This option is important as it will affect how the quick checkout functions when a user clicks it on the product post.', 'wqc' ),
				'options'  => array(
					'lightbox' => __( 'Open checkout in a WooCommerce lightbox', 'wqc' ),
					'reveal'   => __( 'Reveal checkout within product post content', 'wqc' )
				),
			),

			array(
				'title'    => __( 'Checkout position', 'wqc' ),
				'desc'     => __( 'This controls where the checkout will be revealed once the user chooses to checkout using the quick cart button. This only applies to when the "Reveal checkout on product post" option is enabled.', 'wqc' ),
				'id'       => 'woocommerce_quick_checkout_product_checkout_display_position',
				'css'      => 'min-width:150px;',
				'default'  => 'woocommerce_after_single_product_summary',
				'type'     => 'select',
				'options'  => array(
					'woocommerce_after_single_product'          => __( 'After single product', 'wqc' ),
					'woocommerce_before_single_product'         => __( 'Before single product', 'wqc' ),
					'woocommerce_after_single_product_summary'  => __( 'After single product summary', 'wqc' ),
					'woocommerce_before_single_product_summary' => __( 'Before single product summary', 'wqc' ),
					'woocommerce_after_main_content'            => __( 'After main content', 'wqc' ),
					'woocommerce_before_main_content'           => __( 'Before main content', 'wqc' ),
					'woocommerce_after_add_to_cart_form'        => __( 'After add to cart form', 'wqc' ),
					'woocommerce_single_product_summary'        => __( 'Within product summary', 'wqc' ),
				),
				'desc_tip' => true,
			),


			array(
				'title'   => __( 'Product Button Text', 'wqc' ),
				'desc'    => '',
				'id'      => 'woocommerce_quick_checkout_product_button_text',
				'default' => __( 'Buy Now', 'wqc' ),
				'type'    => 'text',
				'css'     => 'min-width:300px;',
			),


			array( 'type' => 'sectionend', 'id' => 'woocommerce_quick_checkout_display_options_product_global' ),


		) );

	}


	/**
	 * Output admin panel
	 */
	public static function admin_panel() {

		if ( ! current_user_can( 'manage_options' ) ) {

			echo '<p>' . __( 'You do not have sufficient permissions to access this page.', 'wqc' ) . '</p>';

		} else {

			//Admin Panel
			global $woocommerce_settings; ?>

			<div class="quick-checkout-wrap">
				<div class="quick-checkout-admin-intro">

					<div class="quick-checkout-logo"></div>

					<p><?php echo sprintf( __( 'Welcome to Quick Checkout for WooCommerce. This plugin expedites the WooCommerce checkout process by allowing users to purchase products on virtually any page with fewer clicks and no page reload. Insert checkout forms directly on single product posts, on the product listing page (also called the catalog/shop page), as well as any other page or post. For more information and instructions please visit <a href="%s" target="_blank">Quick Checkout Documentation</a>.', 'wqc' ), 'http://wordimpress.com/docs/woocommerce-quick-checkout/' )?></p>

				</div>

				<?php
				/**
				 * Woo Options
				 */
				self::add_settings_fields();

				woocommerce_admin_fields( $woocommerce_settings['quick_checkout'] );

				?>

			</div><!--/.quick-checkout-wrap -->
		<?php

		}
	}


	public static function process_admin_options() {
		global $woocommerce_settings;

		self::add_settings_fields();

		woocommerce_update_options( $woocommerce_settings['quick_checkout'] );

	}
}

//WC_Quick_Checkout_Admin::init();


return new WC_Quick_Checkout_Admin();