<?php
/**
 *  Quick Checkout Product Class
 *
 * @description: Responsible for product pages
 * @since      : 1.0
 * @created    : 2/4/14
 */

class Quick_Checkout_Product {

	public function __construct() {

		//Get options from settings
		$this->quick_checkout           = new WC_Quick_Checkout();
		$this->product_option           = get_option( 'woocommerce_quick_checkout_product' );
		$this->product_action           = get_option( 'woocommerce_quick_checkout_product_action' );
		$this->product_button_display   = get_option( 'woocommerce_quick_checkout_product_button_display' );
		$this->product_button_text      = get_option( 'woocommerce_quick_checkout_product_button_text' );
		$this->product_checkout_display = get_option( 'woocommerce_quick_checkout_product_checkout_display_position' );
		$this->product_clear_cart       = get_option( 'woocommerce_quick_checkout_product_cart_action' );
		$this->product_image_button     = get_option( 'woocommerce_quick_checkout_product_image_button' );
		$this->enable_single_control    = get_option( 'woocommerce_quick_checkout_product_metabox' );

		add_action( 'wp', array( $this, 'quick_checkout_product_init' ) );

	}


	/**
	 * Product Init
	 *
	 * Executes on product page init
	 */
	function quick_checkout_product_init() {

		global $post;
		$product = get_product( $post->ID );

		//Does user want quick cart enabled globally and not disabled on an individual post level
		if ( $this->is_quick_checkout_enabled_single() == true ) {

			//get single option for display from post meta
			$single_display_option_override = get_post_meta( $post->ID, 'qc_single_display_option', true );

			//determine which option is enabled on the product and global level and hook action
			//@TODO: There are some pretty nasty conditionals in this switch/case that may need optimization...
			switch ( true ) {

				//Display quick checkout button after
				case ( $this->product_button_display == 'after' && $single_display_option_override == '' || $this->product_button_display == 'after' && $single_display_option_override == 'default' || $single_display_option_override == 'after' ):

					//Determine action to hook depending on product type
					if ( $product->product_type == 'variable' ) {
						$action = 'woocommerce_after_single_variation';
					} else {
						$action = 'woocommerce_after_add_to_cart_button';
					}

					add_action( $action, array( $this, 'quick_checkout_product_button' ) );

					break;

				//Display before
				case ( $this->product_button_display == 'before' && $single_display_option_override == '' || $this->product_button_display == 'before' && $single_display_option_override == 'default' || $single_display_option_override == 'before' ):

					//Determine action to hook depending on product type
					if ( $product->product_type == 'variable' ) {
						$action = 'woocommerce_before_single_variation';
					} else {
						$action = 'woocommerce_before_add_to_cart_button';
					}

					add_action( $action, array( $this, 'quick_checkout_product_button' ) );

					break;

				//Replace button entirely
				case ( $this->product_button_display == 'replace' && $single_display_option_override == '' || $this->product_button_display == 'replace' && $single_display_option_override == 'default' || $single_display_option_override == 'replace' ):

					$this->product_button_display = 'replace';

					//add button before and hide normal button with CSS
					add_action( 'woocommerce_after_add_to_cart_button', array( $this, 'quick_checkout_product_button' ) );

					break;

			} //end switch case


			/**
			 * Product Image Hover Button
			 */
			$single_product_image_button = get_post_meta( $post->ID, 'qc_single_product_image_button', true );

			if ( $this->product_image_button == 'yes' && $single_product_image_button == '' || $single_product_image_button == 'yes' ) {

				add_action( 'woocommerce_product_thumbnails', array( $this, 'quick_checkout_product_image_button' ) );

			}


			//get single option for display from post meta
			$single_product_action_override = get_post_meta( $post->ID, 'qc_single_product_action', true );

			/**
			 * Product Action - Get Checkout Action
			 *
			 * Depends on the option provided by the user globally and per product
			 */
			switch ( true ) {

				//Lightbox option
				case ( $this->product_action == 'lightbox' && $single_product_action_override == '' || $this->product_action == 'lightbox' && $single_product_action_override == 'default' || $single_product_action_override == 'lightbox' ):

					add_action( 'woocommerce_after_single_product', array( $this->quick_checkout, 'quick_checkout_get_checkout' ) );
					break;

				//Display on page
				case ( $this->product_action == 'reveal' && $single_product_action_override == '' || $this->product_action == 'reveal' && $single_product_action_override == 'default' || $single_product_action_override == 'reveal' ):

					//Custom Single Position?
					$single_product_checkout_display = get_post_meta( $post->ID, 'qc_single_product_checkout_display_position', true );

					if ( ! empty( $single_product_checkout_display ) && $single_product_checkout_display !== 'default' ) {
						$this->product_checkout_display = $single_product_checkout_display;
					}

					add_action( $this->product_checkout_display, array( $this->quick_checkout, 'quick_checkout_get_checkout' ) );

					break;

			}


		} //endif

	}


	function quick_checkout_product_button() {

		global $post, $product;

		//Add classes for JS and CSS
		$classes                        = 'quick-checkout-product button alt';
		$single_display_option_override = get_post_meta( $post->ID, 'qc_single_display_option', true );
		$single_product_action_override = get_post_meta( $post->ID, 'qc_single_product_action', true );
		$single_product_cart_action     = get_post_meta( $post->ID, 'qc_single_cart_action', true );
		$single_product_button_text     = get_post_meta( $post->ID, 'qc_single_product_button_text', true );

		//Is there customized button text for this product?
		if ( ! empty( $single_product_button_text ) && $single_product_button_text !== 'Buy Now' ) {
			$this->product_button_text = $single_product_button_text;
		}

		//@TODO: Simplify if/else
		if ( $this->product_button_display == 'after' && $single_display_option_override == '' || $this->product_button_display == 'after' && $single_display_option_override == 'default' || $single_display_option_override == 'after' ) {
			//After class
			$classes .= ' quick-checkout-product-after';
		} elseif ( $this->product_button_display == 'before' && $single_display_option_override == '' || $this->product_button_display == 'before' && $single_display_option_override == 'default' || $single_display_option_override == 'before' ) {
			//Before class
			$classes .= ' quick-checkout-product-before';
		} elseif ( $this->product_button_display == 'replace' && $single_display_option_override == '' || $this->product_button_display == 'replace' && $single_display_option_override == 'default' || $single_display_option_override == 'replace' ) {
			//Replace
			$classes .= ' quick-checkout-product-replace';
		}

		//Echo button (err, link...)
		echo '<a href="#quick-checkout" class="' . $classes . '" data-product-action-override="' . $single_product_action_override . '"  data-product-single-cart-action="' . $single_product_cart_action . '" data-product_id="' . esc_attr( $product->id ) . '">' . apply_filters( 'quick_checkout_product_button_text', $this->product_button_text ) . '</a>';

	}


	public function quick_checkout_product_image_button() {

		global $product, $post;

		$single_product_action_override = get_post_meta( $post->ID, 'qc_single_product_action', true );
		$single_product_button_text     = get_post_meta( $post->ID, 'qc_single_product_button_text', true );
		$single_product_cart_action     = get_post_meta( $post->ID, 'qc_single_cart_action', true );


		//Is there customized button text for this product?
		if ( ! empty( $single_product_button_text ) && $single_product_button_text !== 'Buy Now' ) {
			$this->product_button_text = $single_product_button_text;
		}

		if ( $product->product_type == 'simple' ) {
			echo '<a href="#quick-checkout" class="quick-checkout-button quick-checkout-product quick-checkout-button-image-overlay quick-checkout-button-overlay-single" data-product-action-override="' . $single_product_action_override . '" data-product-single-cart-action="' . $single_product_cart_action . '" data-product_id="' . esc_attr( $product->id ) . '" data-product_sku="' . esc_attr( $product->get_sku() ) . '">' . apply_filters( 'quick_checkout_product_image_button_text', $this->product_button_text ) . '</a>';

		}

	}


	/**
	 * Check whether single product has Quick Checkout enabled or not
	 *
	 * @return bool
	 */
	function is_quick_checkout_enabled_single() {

		global $post;
		$enable_quick_checkout  = get_post_meta( $post->ID, 'qc_enable_checkbox', true );
		$disable_quick_checkout = get_post_meta( $post->ID, 'qc_disable_checkbox', true );


		$response = false;

		//if enabled checkbox is checked  = true
		if ( $enable_quick_checkout == 'on' ) {
			$response = true;
		}

		//if enable/disabled missing and global option turned on = true
		if ( empty( $enable_quick_checkout ) && empty( $disable_quick_checkout ) && $this->product_option == 'yes' ) {
			$response = true;
		}

		//if metabox is not enabled and global options not enabled
		if ( $this->product_option !== 'yes' && $this->enable_single_control !== 'yes' ) {
			$response = false;
		}

		return $response;

	}


}