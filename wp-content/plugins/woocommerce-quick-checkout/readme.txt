=== WooCommerce Quick Checkout ===
Contributors: dlocc, wordimpress
Donate link: http://wordimpress.com/
Tags: woocommerce, checkout, woocommerce checkout, woocommerce single page checkout
Requires at least: 3.6
Tested up to: 3.9.1
Stable tag: 1.5.6

Streamline the WooCommerce checkout process with single page checkout screens and more.

== Description ==

Easily decrease the time it takes to complete a purchase in WooCommerce by providing faster options to checkout. Reduce abandoned carts and increase sales with Quick Checkout.

= Disclaimer =

Be sure to test your checkout forms thoroughly. While we've done our best to code this plugin for use in a variety of website environments, we provide this code "as-is" and make no warranties, representations, covenants or guarantees with regard to the checkout enhancements, tools, and functionalities and will not be liable for any direct, indirect, incidental or consequential damages or for loss of profit, revenue, data, business or use arising out of your use of the this plugin.

The developer of this plugin is in no way affiliated with WooCommerce, WooThemes, WordPress, the company or its affiliates.

== Installation ==

== Frequently Asked Questions ==

= Why should I use this plugin =

The default checkout process in WooCommerce can be slow and tedious. The number of clicks and redirects can lead to higher than expected abandoned cart rates. With Quick Checkout your users can checkout directly on page without any unnecessary redirect or clicks.

= Does this plugin work with my theme? =

If your theme is WooCommerce compatible this plugin should have no issues integrating. The plugin uses standard WooCommerce style classes to display buttons and can be easily adapted to suit your design needs.

== Roadmap ==

= 1.6 =
* New: Subscription support for Shop page
* Fix: Subscription product type support to replace the "Add to Cart" aka "Join Now" button

== Changelog ==

= 1.5.6 =
* Update: For product variations display variation name, not slug, in checkout screen
* Fix: [reveal_quick_checkout] shortcode support for variations, updated plugin docs
* Fix: If variation of a product is not in stock then hide the Quick Checkout button on single product posts
* Fix: If simple product is out of stock do not display Quick Checkout image overlay button on shop page

= 1.5.5 =
* New: [reveal_quick_checkout] now supports quantities with the new "quantity" option
* New: 'quick_checkout_shop_hook' allows for modification of Checkout output for shop pages that may help compatibility with some themes; particularly, ones that use AJAX to load the shop page and don't account for Woo's before/after shop hook
* Fix: Variable type products now validate on the single product posts as expected
* Improved: More reliable mouse hover display for the buy now button that appears over the product image
* Fix: Resolved several PHP warnings for Strict Standards

= 1.5.4 =
* Fix: added conditional to check for is_checkout function to prevent fatal error on plugin activation

= 1.5.3 =
* Fix: reworked how QC works with WooCommerce to output necessary checkout scripts
* Fix: Ensure normal WC checkout page is always reachable
* Fix: Single product post gateways now properly output required scripts
* Improved: Image hover button code output for [product_quick_checkout]

= 1.5.2 =
* New: [reveal_quick_checkout] shortcode now supports variations!
* Improved: AJAX loading of [show_quick_checkout] shortcode that outputs a checkout on page
* Fixed: Subscription checkout didn't properly require account
* Fixed: Issue with [reveal_quick_checkout] displaying "Session expired..." error for non-variation products
* Fixed: Issue with Payment Gateways loading of scripts for non-checkout pages

= 1.5.1 =
* Fix: Conditional check for subsciption type products now actually in place

= 1.5 =
* Improvement: JS handling of centering "Buy Now" button on product images
* New: Support for Subscription Products! Now you can use Quick Checkout with your subscription products
* New: Added addition conditional check for subscription type products for [reveal_quick_checkout] button output
* Fix: Lightbox modal window not opening for Gravity Forms products for non-users (anyone not logged in with proper permissions)
* Fix: Issue where the login form was incorrectly set with display none
* Code cleanup: Removed unnecessary order_item_meta function

= 1.4 =
* Fixed issue with multiple shortcodes on a single page
* SSL: Improved handling of AJAX when SSL is enabled.
* Improved: Body class outputs 'woocommerce' now sitewide, not just when shortcode is detected in the post content. This improves reliability and the checkout styles.

= 1.3 =
* New: Gravity Forms WooCommerce extension compatiblity is here... Hooray!
* New: Added new output location for checkout located after the cart form on single product posts (uses wc's woocommerce_after_add_to_cart_form action to hook)
* Improved: Better and more reliable handling of variation products
* Fix: Bug with Clear Cart before product is added when the Replace Add to Cart button is enabled
* Fix: Prevent Shop image overlay button from displaying on Gravity Form extension enabled products

= 1.2.1 =
* New: [show_quick_checkout] now supports variation by id
* Improved: Various inline documentation cleanup

= 1.2 =
* Updated: Text for various options and settings to add more description and clarity
* Updated: Added license activations remaining stat to license and improved various license styles
* Updated: Added license expired text for licenses that have gone over their activation date
* Improved: Reworked how the "Buy Now" button gets placed over the image so it's more reliable

= 1.1 =
* New: Shortcode to add checkout directly onto page with a product already in the cart ready for immediate checkout
* Updated: Readme.txt added more content

= 1.0.1 =
* Fix: Plugin does not activate unless WooCommerce version 2.1+ installed and active
* Fix: Minor PHP warning when trying to deactivate plugin with WooCommerce still active
* Fix: Minor PHP Warning when plugin activated illegal string offset

= 1.0 =
* Plugin release