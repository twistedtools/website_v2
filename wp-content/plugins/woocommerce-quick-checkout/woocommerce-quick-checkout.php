<?php
/*
Plugin Name: WooCommerce Quick Checkout
Plugin URI: http://wordimpress.com/
Description: Single page checkout process for WooCommerce to expedite the checkout process and increase conversion rates
Version: 1.5.6
Author: WordImpress, Devin Walker
Author URI: http://wordimpress.com/
License: GPLv2
*/

// Define Constants
define( 'WQC_PLUGIN_PATH', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'WQC_PLUGIN_URL', plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) );
define( 'WQC_PLUGIN_BASE', plugin_basename( __FILE__ ) );
define( 'WQC_DEBUG', false );

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


if ( ! class_exists( 'WC_Quick_Checkout' ) ) {

	class WC_Quick_Checkout {

		/**
		 * @var WooCommerce The single instance of the class
		 * @since 1.0
		 */
		protected static $_instance = null;

		public $min_woocommerce_version = '2.1';
		public $plugin_path = null;
		public $quick_checkout_shop = null;
		public $quick_checkout_product = null;
		public $quick_checkout_shortcodes = null;
		public $shortcode_flag = null;
		public $licence = null;
		public $licence_key = null;
		public $checkout_page_id = null;

		/**
		 * Constructor
		 *
		 * @param        $file
		 */
		public function __construct() {

			global $woocommerce;

			$this->file        = __FILE__;
			$this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
			$this->plugin_url  = WP_PLUGIN_URL . '/' . str_replace( basename( __FILE__ ), '', plugin_basename( __FILE__ ) );
			$this->store_url   = 'http://wordimpress.com';
			$this->item_name   = 'WooCommerce Quick Checkout';

			add_action( 'plugins_loaded', array( $this, 'version_check' ), 10 );

			// Loaded action
			do_action( 'woocommerce_quick_checkout_loaded' );

		}

		/**
		 * Main WC Quick Checkout Instance
		 *
		 * Ensures only one instance of WC Quick Checkout is loaded or can be loaded.
		 *
		 * @since 1.0
		 * @static
		 * @see   WC()
		 * @return WooCommerce - Main instance
		 */
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}

			return self::$_instance;
		}


		/**
		 * Check WooCommerce Version
		 *
		 * Require at least WooCommerce verions 2.1
		 */
		public function version_check() {
			global $woocommerce;

			if ( is_object( $woocommerce ) && version_compare( $woocommerce->version, $this->min_woocommerce_version ) >= 0 ) {
				$this->init();
			} else {
				add_action( 'admin_notices', array( &$this, 'admin_notice' ) );
			}

		}

		/**
		 * Display Update Notice
		 */
		public function admin_notice() {
			echo '<div class="error">
     		    <p>' . sprintf( __( 'The <strong>WooCommerce Quick Checkout</strong> extension requires at least WooCommerce %s in order to function properly. Please install or upgrade the WooCommerce plugin in order to use the WooCommerce Quick Checkout plugin.', 'wqc' ), $this->min_woocommerce_version ) . '</p>
     		</div>';
			//Deactivate this beast
			deactivate_plugins( plugin_basename( __FILE__ ) );
		}


		/**
		 * Plugin Initialization
		 */
		public function init() {

			// Require admin class to handle all backend functions
			if ( is_admin() ) {
				require_once( dirname( __FILE__ ) . '/classes/class-woocommerce-quick-checkout-admin.php' );

				//Licensing
				require_once( dirname( __FILE__ ) . '/includes/license/licence.php' );

				//Licence Args
				$licence_args = array(
					'plugin_basename'     => WQC_PLUGIN_BASE,
					'settings_page'       => 'settings_page_quick-checkout-license', //Used to determine CSS enqueues
					'store_url'           => $this->store_url,
					'item_name'           => $this->item_name,
					'licence_key_setting' => 'wqc_licence_setting',
					'licence_key_option'  => 'edd_quick_checkout_license_key',
					'licence_key_status'  => 'edd_quick_checkout_license_status',
				);

				$current_options   = get_option( $licence_args['licence_key_option'] );
				$this->licence_key = ! empty( $current_options ) ? trim( $current_options['license_key'] ) : '';

				$this->licence = new Quick_Checkout_Licence( $licence_args );
				add_action( 'admin_init', array( $this, 'edd_sl_wordimpress_updater' ) );


			} else {
				// Load classes/includes
				require_once( dirname( __FILE__ ) . '/classes/class-woocommerce-quick-checkout-shop.php' );
				require_once( dirname( __FILE__ ) . '/classes/class-woocommerce-quick-checkout-product.php' );
				require_once( dirname( __FILE__ ) . '/classes/class-woocommerce-quick-checkout-shortcodes.php' );

				//Class instances
				$this->quick_checkout_shop       = new Quick_Checkout_Shop();
				$this->quick_checkout_product    = new Quick_Checkout_Product();
				$this->quick_checkout_shortcodes = new Quick_Checkout_Shortcodes();

			}


			//SSL
			add_action( 'template_redirect', array( $this, 'quick_checkout_ssl_template_redirect' ), 1 );

			//AJAX
			add_action( 'wp_ajax_quick_checkout_action', array( $this, 'quick_checkout_action_callback' ) );
			add_action( 'wp_ajax_nopriv_quick_checkout_action', array( $this, 'quick_checkout_action_callback' ) );

			//Enqueue Scripts/Styles
			add_action( 'wp_enqueue_scripts', array( $this, 'quick_checkout_enqueue' ) );


			//This is the actual checkout page ID before we manipulate the is_checkout conditional
			$this->checkout_page_id = wc_get_page_id( 'checkout' );
			//Remove WC's template redirect and replace with QC's
			add_action( 'wp_loaded', array( $this, 'remove_action_template_redirect' ) );
			//Add QC's template redirects that play friendly with QC
			add_action( 'template_redirect', array( $this, 'wc_template_redirect' ) );

		}


		/**
		 * Handle redirects before content is output - hooked into template_redirect so is_page works.
		 *
		 * See: woocommerce/includes/wc-template-functions.php
		 * @return void
		 */
		function wc_template_redirect() {

			global $wp_query, $wp;

			// When default permalinks are enabled, redirect shop page to post type archive url
			if ( ! empty( $_GET['page_id'] ) && get_option( 'permalink_structure' ) == "" && $_GET['page_id'] == wc_get_page_id( 'shop' ) ) {
				wp_safe_redirect( get_post_type_archive_link( 'product' ) );
				exit;
			} // When on the checkout with an empty cart, redirect to cart page
			elseif ( is_page( $this->checkout_page_id ) && sizeof( WC()->cart->get_cart() ) == 0 && empty( $wp->query_vars['order-pay'] ) && ! isset( $wp->query_vars['order-received'] ) ) {

				wp_redirect( get_permalink( wc_get_page_id( 'cart' ) ) );
				exit;
			} // Logout
			elseif ( isset( $wp->query_vars['customer-logout'] ) ) {
				wp_redirect( str_replace( '&amp;', '&', wp_logout_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ) ) );
				exit;
			} // Redirect to the product page if we have a single product
			elseif ( is_search() && is_post_type_archive( 'product' ) && apply_filters( 'woocommerce_redirect_single_search_result', true ) && $wp_query->post_count == 1 ) {
				$product = get_product( $wp_query->post );

				if ( $product->is_visible() ) {
					wp_safe_redirect( get_permalink( $product->id ), 302 );
					exit;
				}
			} // Ensure payment gateways are loaded early
			elseif ( is_add_payment_method_page() ) {

				WC()->payment_gateways();

			} // Checkout pages handling
			elseif ( is_checkout() || is_page( $this->checkout_page_id ) ) {
				// Buffer the checkout page
				ob_start();

				// Ensure gateways and shipping methods are loaded early
				WC()->payment_gateways();
				WC()->shipping();
			}

		}

		/**
		 * Remove WC's Template Redirect
		 * We replace the built in function with QC's which plays nicely with non-checkout page checkouts
		 */
		function remove_action_template_redirect() {
			remove_action( 'template_redirect', 'wc_template_redirect' );
		}


		/**
		 * Plugin Updates
		 *
		 * Provides native plugin updates for users with valid, activated license keys
		 */
		function edd_sl_wordimpress_updater() {

			$meta = get_plugin_data( $this->file, false );

			// Include Licensing
			if ( ! class_exists( 'EDD_SL_Plugin_Updater' ) ) {
				// load our custom updater
				include_once( dirname( __FILE__ ) . '/includes/license/classes/EDD_SL_Plugin_Updater.php' );
			}

			// setup the updater
			$edd_updater = new EDD_SL_Plugin_Updater( $this->store_url, WQC_PLUGIN_BASE, array(
					'version'   => $meta["Version"], // current version number
					'license'   => $this->licence_key, // license key (used get_option above to retrieve from DB)
					'item_name' => $this->item_name, // name of this plugin
					'author'    => 'Devin Walker' // author of this plugin
				)
			);

		}


		/**
		 * Adds JS params for WooCommerce AJAX
		 *
		 * This func actually sets the $woocommerce_params if not on checkout page
		 */
		function quick_checkout_add_woocommerce_params( $woocommerce_params ) {

			global $woocommerce;

			if ( ! is_checkout() ) {
				if ( empty( $woocommerce_params['locale'] ) ) {
					$woocommerce_params['locale'] = json_encode( $woocommerce->countries->get_country_locale() );
				}
				$woocommerce_params['is_checkout'] = '1';

				return $woocommerce_params;
			}

			return '';

		}


		/**
		 * Scripts / Styles
		 *
		 * Used to enqueue scripts in on the frontend
		 *
		 */
		function quick_checkout_enqueue() {

			global $wp, $wp_scripts;

			$suffix = defined( 'WQC_DEBUG' ) && WQC_DEBUG ? '' : '.min';

			//Enqueue WooCommerce cart checkout js
			//lots of code borrowed from woocommerce/includes/class-wc-frontend-scripts.php
			$assets_path          = str_replace( array( 'http:', 'https:' ), '', WC()->plugin_url() ) . '/assets/';
			$frontend_script_path = $assets_path . 'js/frontend/';

			wp_enqueue_script( 'wc-checkout', $frontend_script_path . 'checkout' . $suffix . '.js', array( 'jquery', 'woocommerce', 'wc-country-select', 'wc-address-i18n' ), WC_VERSION, true );

			//WooCommerce doesn't set checkout params unless on an actual checkout page,
			//this fixes that issue and prevents various JS errors from breaking other functionality
			wp_localize_script(
				'wc-checkout', 'wc_checkout_params', apply_filters(
					'wc_checkout_params', array(
						'ajax_url'                  => WC()->ajax_url(),
						'ajax_loader_url'           => apply_filters( 'woocommerce_ajax_loader_url', $assets_path . 'images/ajax-loader@2x.gif' ),
						'update_order_review_nonce' => wp_create_nonce( "update-order-review" ),
						'apply_coupon_nonce'        => wp_create_nonce( "apply-coupon" ),
						'option_guest_checkout'     => get_option( 'woocommerce_enable_guest_checkout' ),
						'checkout_url'              => add_query_arg( 'action', 'woocommerce_checkout', WC()->ajax_url() ),
						'is_checkout'               => empty( $wp->query_vars['order-pay'] ) && ! isset( $wp->query_vars['order-received'] ) ? 1 : 0
					)
				)
			);


			//if debugging enqueue non-min assets
			if ( WQC_DEBUG == true ) {
				wp_enqueue_script( 'quick_checkout_magnific_popup_js', plugins_url( '/assets/js/jquery.magnific.popup' . $suffix . '.js', __FILE__ ), array( 'jquery', 'woocommerce' ), false, true );
				wp_enqueue_style( 'quick_checkout__magnific_popup_css', plugins_url( '/assets/css/magnific-popup' . $suffix . '.css', __FILE__ ) );

				//Quick Checkout Scripts
				wp_enqueue_script( 'quick_checkout_scripts', plugins_url( '/assets/js/quick-checkout-ajax' . $suffix . '.js', __FILE__ ), array( 'jquery', 'woocommerce' ), false, true );

			} //Not debugging so enqueue single minified JS
			else {
				//One JS file to rule them all
				wp_enqueue_script( 'quick_checkout_scripts', plugins_url( '/assets/js/quick-checkout.min.js', __FILE__ ), array( 'jquery', 'woocommerce' ), false, true );

			}

			//Quick Checkout CSS
			wp_enqueue_style( 'quick_checkout_css', plugins_url( '/assets/css/quick-checkout' . $suffix . '.css', __FILE__ ) );

			//Quick Checkout AJAX params
			// in javascript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
			wp_localize_script(
				'quick_checkout_scripts', 'quick_checkout',
				array(
					'ajax_url'                    => WC()->ajax_url(),
					'shop_on'                     => $this->quick_checkout_shop->shop_option,
					'shop_checkout_action'        => $this->quick_checkout_shop->shop_action,
					'shop_clear_cart'             => $this->quick_checkout_shop->shop_clear_cart,
					'shop_cart_reveal'            => $this->quick_checkout_shop->shop_cart_reveal,
					'shop_cart_reveal_text'       => $this->quick_checkout_shop->shop_cart_reveal_text,
					'product_checkout_action'     => $this->quick_checkout_product->product_action,
					'product_button_display'      => $this->quick_checkout_product->product_button_display,
					'product_clear_cart'          => $this->quick_checkout_product->product_clear_cart,
					'woo_checkout_js_url'         => $wp_scripts->registered["wc-checkout"]->src,
					'woo_checkout_country_js_url' => $wp_scripts->registered["wc-country-select"]->src,
					'woocommerce_is_cart'         => is_cart(),
					'woocommerce_is_checkout'     => is_checkout(),
					'woocommerce_is_shop'         => is_shop(),
				)
			);


		}


		/**
		 * AJAX Callback Function
		 */
		public function quick_checkout_action_callback() {

			global $woocommerce;

			//Add-on Support
			//Plugins like Subscription Add-on modify form fields
			do_action( 'woocommerce_checkout_fields' ); //Fire off checkout fields action for Add-on support

			//Clear cart
			//Important that this runs first
			if ( isset( $_POST['clear_cart'] ) && $_POST['clear_cart'] == 'yes' ) {
				$woocommerce->cart->empty_cart();
			}

			//Add item to cart
			if ( isset( $_POST['shop_add_to_cart'] ) && $_POST['shop_add_to_cart'] !== 'false' && isset( $_POST['product_id'] ) ) {

				$product_args = isset( $_POST['shop_add_to_cart'] ) ? wp_parse_args( $_POST['shop_add_to_cart'] ) : '';
				$quantity     = isset( $product_args['quantity'] ) ? $product_args['quantity'] : 1;

				$woocommerce->cart->add_to_cart( $_POST['product_id'] );

			}

			/**
			 * Add Single Products to Cart
			 *
			 * @see:
			 * http://docs.woothemes.com/document/automatically-add-product-to-cart-on-visit/
			 * http://wordpress.stackexchange.com/questions/53280/woocommerce-add-a-product-to-cart-programmatically-via-js-or-php
			 */
			if ( isset( $_POST['product_add_to_cart'] ) ) {

				//extract get product args
				$product_args = isset( $_POST['product_add_to_cart'] ) ? wp_parse_args( $_POST['product_add_to_cart'] ) : '';

				//Set variables
				$product_id      = isset( $product_args['add-to-cart'] ) ? $product_args['add-to-cart'] : 1;
				$product         = get_product( $product_id );
				$variation_id    = isset( $product_args['variation_id'] ) ? $product_args['variation_id'] : '';
				$variation       = array();
				$quantity        = isset( $product_args['quantity'] ) ? $product_args['quantity'] : 1;
				$gravity_form_id = isset( $product_args['gform_form_id'] ) ? $product_args['gform_form_id'] : '';


				//Support for $_POST quantity
				if ( isset( $_POST['quantity'] ) && ! isset( $product_args['quantity'] ) ) {
					$quantity = $_POST['quantity'];
				}


				//Push all product args to $_POST variable for variation support
				foreach ( $product_args as $key => $value ) {

					if ( 'attribute_' == substr( $key, 0, 10 ) ) {
						//Workaround to deslugify the attribute name
						$value     = str_replace( array( '-', '_' ), ' ', $value );
						$variation = $variation + array(
								$key => ucwords( $value )
							);
					} //Gravity Forms and Donation + perhaps other add-on support
					//those plugin expect $_POST filled with $product_args
					else {
						//little hack for Gravity Forms to work with AJAX
						$_POST = $_POST + array(
								$key => $value
							);
					}

				} //end foreach

				//Gravity Forms Support
				if ( $gravity_form_id && class_exists( 'GFCommon' ) ) {

					wp_set_current_user( 1 ); //Set to a user with permissions to submit Gform (see save_lead() )
					$gforms_addon = new woocommerce_gravityforms();
					$gforms_addon->add_cart_item_data( $_POST, $product_id );

					//If gform is invalid pass on invalid to JS and exit function
					if ( ! GFFormDisplay::$submission[$gravity_form_id]['is_valid'] ) {
						echo "invalid";
						exit;
					}
				}


				//Single Product Post: Variable product Validation Support
				if ( $product->is_type( 'variable' ) ) {

					//Validate that all variations are in place or send invalid response
					if ( empty( $variation_id ) ) {

						echo "invalid";
						exit;

					}


				}

				WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation );

			}


			/**
			 * Shortcode Reveal Add Variation
			 *
			 * Used with the [reveal_quick_checkout] to support variable products add to cart on click
			 */

			if ( isset( $_POST['reveal_checkout_add_variation'] ) ) {
				$variation    = array();
				$product_id   = isset( $_POST['add-to-cart'] ) ? $_POST['add-to-cart'] : 1;
				$variation_id = isset( $_POST['variation_id'] ) ? $_POST['variation_id'] : '';
				$quantity     = isset( $_POST['quantity'] ) ? $_POST['quantity'] : 1;

				//Shortcode support for variation ID usage
				if ( ! empty( $variation_id ) ) {

					$product = new WC_Product_Variable( $product_id );

					//get attribute name for this variation ID
					$variations = $product->get_available_variations();

					foreach ( $variations as $id => $data ) {

						//we have a match
						if ( $data['variation_id'] === intval( $variation_id ) ) {

							$attributes = $data['attributes'];

							//create $variation data array
							foreach ( $attributes as $name => $value ) {

								//Workaround to deslugify the attribute name
								$value = str_replace( array( '-', '_' ), ' ', $value );

								$variation = $variation + array(
										$name => ucwords( $value )
									);
							}

						}


					}

				}

				WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation );
			}


			/**
			 * Checkout Add Variation
			 *
			 * Used with the [show_checkout_shortcode] to add variable products to cart on page load
			 *
			 */
			if ( isset( $_POST['checkout_add_variation'] ) ) {

				//Set variables
				$product_id   = isset( $_POST['product_id'] ) ? $_POST['product_id'] : '';
				$variation_id = isset( $_POST['variation_id'] ) ? $_POST['variation_id'] : '';
				$quantity     = isset( $_POST['quantity'] ) ? $_POST['quantity'] : 1;

				$variation = '';

				//Get Product Data
				if ( ! empty( $variation_id ) ) {
					$variation_data = get_post_meta( $variation_id );

					//Get variation from variation data
					foreach ( $variation_data as $key => $value ) {

						if ( 'attribute_' == substr( $key, 0, 10 ) && ! empty( $value[0] ) ) {
							$variation = array( $key => $value[0] );
						}
					}
				}

				WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation );

			}
			/**
			 * Get Checkout
			 *
			 * Returns an updated checkout screen for AJAX display on frontend
			 *
			 */
			if ( isset( $_POST['get_checkout'] ) ) {

				$this->quick_checkout_get_checkout();

			}

			die();

		}


		/**
		 * Get the WooCommerce checkout
		 *
		 * This function is responsible for placing a WooCommerce checkout on page/post
		 * It includes filters and actions to customize the pre and post checkout content
		 * as well as modify the opening and closing HTML tags
		 */
		public function quick_checkout_get_checkout() {

			global $woocommerce;

			//Proceed if Woo Checkout constant not defined
			if ( ! defined( 'WOOCOMMERCE_CHECKOUT' ) ) {

				define( 'WOOCOMMERCE_CHECKOUT', true );

				wc_print_notices();

				// Calc totals
				WC()->cart->calculate_totals();

				// Check cart contents for errors
				do_action( 'woocommerce_check_cart_items' );

				// Get checkout object
				$checkout = WC()->checkout();

				//Main wrap w/ filter to allow for user modification
				echo apply_filters( 'quick_checkout_opening_tag', '<div id="quick-checkout"><div class="quick-checkout-overlay"></div>' );

				if ( wc_notice_count() == 0 && isset( $non_js_checkout ) ) {
					$woocommerce->add_message( __( 'The order totals have been updated. Please confirm your order by pressing the Place Order button at the bottom of the page.', 'woocommerce' ) );
				}

				//pre checkout action
				do_action( 'quick_checkout_pre_checkout' );

				//get the checkout template
				woocommerce_get_template( 'checkout/form-checkout.php', array( 'checkout' => $checkout ) );

				//post checkout action
				do_action( 'quick_checkout_post_checkout' );

				//Closing tag w/ filter to allow for user modification
				echo apply_filters( 'quick_checkout_closing_tag', '</div>' );


			}

		}

		/**
		 * Force SSL.
		 *
		 * Force SSL on Certain Pages in WooCommerce
		 * Description: modified version of Woocommerce SSL functionality, forces ssl on Woocommerce pages and two additional custom pages
		 *
		 */
		function quick_checkout_ssl_template_redirect() {

			//user wants SSL turned on for shopping cart
			if ( get_option( 'woocommerce_quick_checkout_shop_ssl' ) === 'yes' ) {


				//user wants SSL turned on for shopping cart
				if ( get_option( 'woocommerce_quick_checkout_shop_ssl' ) === 'yes' && ! is_ssl() ) {

					if ( is_shop() || is_product() ) {

						if ( 0 === strpos( $_SERVER['REQUEST_URI'], 'http' ) ) {

							wp_safe_redirect( preg_replace( '|^http://|', 'https://', $_SERVER['REQUEST_URI'] ) );

							exit;

						} else {

							wp_safe_redirect( 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );

							exit;

						}

					}

				} // Break out of SSL if we leave woocommerce pages or custom pages

				elseif ( is_ssl() && $_SERVER['REQUEST_URI'] && ! is_shop() && ! is_product() && ! is_woocommerce() && ! is_checkout() ) {

					//If not using SSL break out

					if ( 0 === strpos( $_SERVER['REQUEST_URI'], 'http' ) ) {

						wp_safe_redirect( preg_replace( '|^https://|', 'http://', $_SERVER['REQUEST_URI'] ) );

						exit;

					} else {

						wp_safe_redirect( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );

						exit;

					}

				}

			}


		} //end function


	}


}


/**
 * Returns the main instance of Quick Checkout to prevent the need to use globals.
 *
 * @since  1.0
 * @return WC_Quick_Checkout
 */
function WC_Quick_Checkout() {
	return WC_Quick_Checkout::instance();
}

WC_Quick_Checkout();


/**
 * Override the WC is_checkout conditional
 *
 * QC does this so WC outputs necessary scripts on single product posts, shop page, etc.
 *
 * @return bool
 */
if ( ! function_exists( 'is_checkout' ) ) {

	function is_checkout() {

		if ( is_product() || is_shop() || is_page() && ! is_cart() ) {
			return true;
		}

		//	return is_page( wc_get_page_id( 'checkout' ) ) ? true : false;
	}
}