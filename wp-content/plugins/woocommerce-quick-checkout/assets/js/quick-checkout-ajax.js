/**
 * WooCommerce QuickCart Checkout JS
 */
(function ( $ ) {
	'use strict';

	//On Doc Ready
	$( document ).ready( function () {

		/**
		 * Shop Page
		 */
		$( document.body ).on( 'click', '.quick-checkout-button-shop', function ( e ) {

			e.preventDefault();

			//Custom Checkout Action?
			var checkout_action_override = $( this ).data( 'product-action-override' );
			var cart_action_override = $( this ).data( 'product-single-cart-action' );

			//Add loading classes
			$( this ).addClass( 'loading' ).prop( 'disabled', true );
			$( this ).closest( '.product' ).addClass( 'quick-checkout-active-loading' );

			//Clear Cart Option
			var clear_cart_option = 'no';
			if ( quick_checkout.shop_clear_cart == 'yes' || cart_action_override == 'on' ) {
				clear_cart_option = 'yes';
			}

			//Add item to cart option
			//do not add for reveal after added to cart link
			var option_add_to_cart = false;
			if ( !$( this ).hasClass( 'quick-checkout-now' ) ) {
				option_add_to_cart = true;
			}

			//AJAX Handle Shop Add_to_cart
			$.post( quick_checkout.ajax_url,
				{
					action          : 'quick_checkout_action',
					clear_cart      : clear_cart_option,
					shop_add_to_cart: option_add_to_cart,
					product_id      : $( this ).data( 'product_id' ),
					product_sku     : $( this ).data( 'product_sku' ),
					get_checkout    : 'true'
				},

				function ( response ) {

					quick_checkout_handle_ajax_response( response );

					//Item added to cart so display checkout
					$( 'body' ).one( 'updated_checkout', function () {

						qc_open_checkout( checkout_action_override, quick_checkout );

						//remove loading classes from clicked button
						$( '.quick-checkout-button-shop' ).removeClass( 'loading' ).prop( 'disabled', false );
						$( '.quick-checkout-button-image-overlay' ).closest( '.product' ).removeClass( 'quick-checkout-active-loading' );

						//fadeOut overlay div
						$( '.quick-checkout-overlay' ).fadeOut( 300 );

					} );


				} ).fail(function () {
					alert( "Error adding product to cart" );
				} ).always( function () {
					console.log( 'AJAX quick_checkout_action complete' );
				} );

			return false;

		} );


		//Single Product Posts Custom Selector
		var extra_selector = '';
		if ( quick_checkout.product_button_display == 'replace' ) {
			extra_selector = ', .single_add_to_cart_button';
		}

		/**
		 * Single Products Posts
		 */
		$( '.quick-checkout-product' + extra_selector ).on( 'click', function ( e ) {
			e.preventDefault();

			//Custom Checkout Action?
			var checkout_action_override = $( this ).data( 'product-action-override' );
			var cart_action_override = $( this ).data( 'product-single-cart-action' );
			//Clear Cart Option
			var clear_cart_option = 'no';
			if ( quick_checkout.product_clear_cart == 'yes' || cart_action_override == 'on' ) {
				clear_cart_option = 'yes';
			}

			//Add loading classes
			$( this ).addClass( 'loading' );
			$( this ).closest( '.images' ).addClass( 'quick-checkout-active-loading' );

			var frm = $( 'form.cart' );
			var data = {
				action             : 'quick_checkout_action',
				clear_cart         : clear_cart_option,
				product_add_to_cart: frm.serialize(),
				get_checkout       : "true"
			};

			//AJAX Handle Shop Add_to_cart
			$.post( quick_checkout.ajax_url, data,

				function ( response ) {

					//Catch gravity form errors
					if ( response == 'invalid' ) {
						$( 'form.cart' ).submit();
						return;
					}

					quick_checkout_handle_ajax_response( response );

					//Item added to cart so display checkout
					$( 'body' ).one( 'updated_checkout', function () {

						qc_open_checkout( checkout_action_override, quick_checkout );

						//remove loading classes from clicked button
						$( '.quick-checkout-product' + extra_selector ).removeClass( 'loading' );

						//fadeOut overlay div
						$( '.quick-checkout-overlay' ).fadeOut( 300 );


					} );


				} ).fail(function () {
					alert( "Error adding product to cart" );
				} ).always( function () {
					console.log( 'AJAX quick_checkout_action complete' );
				} );

			return false;

		} );


		//Shortcode JS
		//Option is a bit different because it uses data attributes
		$( '.quick-checkout-button-shortcode' ).on( 'click', function ( e ) {

			e.preventDefault();
			var data;
			var checkout_action = $( this ).data( 'checkout-action' );

			//Add loading classes
			$( this ).addClass( 'loading' );
			$( this ).closest( '.product' ).addClass( 'quick-checkout-active-loading' );

			//Clear Cart Option
			var clear_cart_option = 'no';
			if ( $( this ).data( 'clear-cart' ) == 'yes' || $( this ).data( 'clear-cart' ) == true ) {
				clear_cart_option = 'yes';
			}

			var variation_id = $( this ).data( 'variation_id' );

			//Does this checkout include a variation?
			if ( typeof variation_id !== 'undefined' && variation_id !== '' ) {
				data = {
					action                        : 'quick_checkout_action',
					clear_cart                    : clear_cart_option,
					reveal_checkout_add_variation : 'true',
					'add-to-cart'                 : $( this ).data( 'product_id' ),
					variation_id                  : variation_id,
					product_sku                   : $( this ).data( 'product_sku' ),
					get_checkout                  : 'true',
					quantity                      : $( this ).data( 'quantity' )
				}
			} else {
				data = {
					action          : 'quick_checkout_action',
					clear_cart      : clear_cart_option,
					shop_add_to_cart: 'true',
					product_id      : $( this ).data( 'product_id' ),
					product_sku     : $( this ).data( 'product_sku' ),
					get_checkout    : 'true',
					quantity        : $( this ).data( 'quantity' )
				}

			}

			//AJAX Handle Shop Add_to_cart
			$.post( quick_checkout.ajax_url, data,

				function ( response ) {

					quick_checkout_handle_ajax_response( response );

					//Item added to cart so display checkout
					$( 'body' ).one( 'updated_checkout', function () {

						//Which option has the user selected to open checkout?
						if ( checkout_action == 'lightbox' ) {
							qc_open_modal();
						}
						//Reveal option
						else {

							//Reveal checkout form
							$( '#quick-checkout' ).slideDown( 'slow' );
							//Scrollto form
							$( 'html, body' ).animate( {
								scrollTop: $( "#quick-checkout" ).offset().top - 100
							}, 500 );

						}

						//remove loading classes from clicked button
						$( '.quick-checkout-button-shortcode' ).removeClass( 'loading' );
						$( '.quick-checkout-button-shortcode' ).closest( '.quick-checkout-active-loading' ).removeClass( 'quick-checkout-active-loading' );

						//fadeOut overlay div
						$( '.quick-checkout-overlay' ).fadeOut( 300 );

					} );

				} ).fail(function () {
					alert( "Error adding product to cart" );
				} ).always( function () {
					console.log( 'AJAX quick_checkout_action complete' );
				} );

			return false;

		} );


	} ); //end on document ready


	/**
	 * Open Checkout Link
	 *
	 * Used for links that can be placed in header or footer that will display a link/button that opens checkout
	 *
	 */
	$( '.quick-checkout-link' ).on( 'click', function ( e ) {

		e.preventDefault();
		var $this = $( this );
		//prevent double-clicks opening and closing checkout
		var alreadyClicked = $this.data( 'clicked' );
		if ( alreadyClicked ) {
			return false;
		}
		$this.data( 'clicked', true );
		$( this ).addClass( 'loading' );

		//Clear Cart Option
		var clear_cart_option = 'no';
		if ( $( this ).data( 'clear-cart' ) == 'yes' || $( this ).data( 'clear-cart' ) == true ) {
			clear_cart_option = 'yes';
		}

		//get Checkout via AJAX
		$.post( quick_checkout.ajax_url,
			{
				action          : 'quick_checkout_action',
				get_checkout    : 'true',
				shop_add_to_cart: 'true',
				clear_cart      : clear_cart_option,
				product_id      : $( this ).data( 'product_id' ),
				product_sku     : $( this ).data( 'product_sku' )
			},

			function ( response ) {

				//Add style classes to quick checkout
				quick_checkout_handle_ajax_response( response );

				//Item added to cart so display checkout
				$( 'body' ).one( 'updated_checkout', function () {

					//Which option has the user selected to open checkout?
					jQuery.magnificPopup.open( {
						items    : {
							src : $( '#quick-checkout' ), // can be a HTML string, jQuery object, or CSS selector
							type: 'inline'
						},
						callbacks: {
							open : function () {
								$( '#quick-checkout' ).addClass( 'white-popup' );
								$( '#quick-checkout' ).show();
								$( '.quick-checkout-overlay' ).fadeOut( 'fast' );
								$( '.quick-checkout-link' ).removeClass( 'loading' );
								$this.data( 'clicked', false ); //reenable link to be clicked
							},
							close: function () {
								// Will fire when popup is closed
							}
							// e.t.c.
						}

					} );


				} );

			} ).fail(function () {
				alert( "Error opening Quick Checkout" );
			} ).always( function () {
				console.log( 'AJAX quick_checkout_action complete' );
			} );

		return false;


	} );

	/**
	 * Add a Checkout Now link after Add to Cart
	 *
	 * Shop and Shortcode Reveals Quick Checkout after Product Added to Cart
	 */
//not on cart page
	if ( quick_checkout.woocommerce_is_cart !== '1' ) {

		//only if options enabled;
		//shop option on, is on shop page, and shop cart reveal on
		//shortcode option yes, not on shop page
		if ( quick_checkout.shop_cart_reveal == 'yes' && quick_checkout.woocommerce_is_shop == '1' && quick_checkout.shop_on == 'yes' || quick_checkout.shortcode_shop_cart_reveal == 'yes' && quick_checkout.woocommerce_is_shop !== '1' ) {

			$( '.add_to_cart_button' ).on( 'click', function () {

				var this_clicked_button = $( this );
				var this_clicked_button_sku = $( this ).data( 'product_sku' );
				var this_clicked_button_id = $( this ).data( 'product_id' );
				var button_text = quick_checkout.shop_cart_reveal_text;

				if ( quick_checkout.shortcode_shop_cart_reveal_text ) {
					button_text = quick_checkout.shortcode_shop_cart_reveal_text;
				}

				$( 'body' ).on( 'added_to_cart', function ( data ) {

					//if this product doesn't already have a quick checkout link append one after
					if ( $( this_clicked_button ).closest( '.product' ).find( '.quick-checkout-now' ).length == 0 ) {

						$( this_clicked_button ).after(
							'<a href="#" class="quick-checkout-now quick-checkout-button-shop wc-forward" data-product_id="' + this_clicked_button_id + '" data-product_sku="' + this_clicked_button_sku + '">' + button_text + '</a>'
						);
					}


				} );

			} );

		}
	}


	/**
	 * Open Checkout
	 * @TODO: Code simplification and cleanup
	 */
	function qc_open_checkout( checkout_action_override, quick_checkout ) {

		//Which action is selected
		if ( quick_checkout.shop_checkout_action == 'lightbox' && checkout_action_override == undefined || quick_checkout.shortcode_checkout_action == 'lightbox' ) {

			//fire open modal function
			qc_open_modal();

		} else if ( quick_checkout.shop_checkout_action == 'reveal' && checkout_action_override == undefined || quick_checkout.shortcode_checkout_action == 'reveal' ) {

			//Reveal checkout form
			$( '#quick-checkout' ).slideDown( 'slow' );
			//Scrollto form
			$( 'html, body' ).animate( {
				scrollTop: $( "#quick-checkout" ).offset().top - 100
			}, 500 );

		}
		//On Product Page Lightbox
		else if ( checkout_action_override == 'lightbox' || quick_checkout.product_checkout_action == 'lightbox' && checkout_action_override == '' || quick_checkout.product_checkout_action == 'lightbox' && checkout_action_override == 'default' ) {

			//fire open modal function
			qc_open_modal();

		}
		//Product Page Reveal
		else if ( checkout_action_override == 'reveal' || quick_checkout.product_checkout_action == 'reveal' && checkout_action_override == '' || quick_checkout.product_checkout_action == 'reveal' && checkout_action_override == 'default' ) {

			//Reveal checkout form
			$( '#quick-checkout' ).slideDown( 'slow' );
			//Scrollto form
			$( 'html, body' ).animate( {
				scrollTop: $( "#quick-checkout" ).offset().top - 100
			}, 500 );

		}
	}

	/**
	 * Add Product to Cart on Page Load
	 *
	 * This function is used on landing pages to add a specific product to the cart for user purchase on load
	 */
	if ( quick_checkout.auto_add_to_cart ) {
		//Clear Cart Option
		var clear_cart_option = 'no';
		if ( quick_checkout.clear_cart == 'true' ) {
			clear_cart_option = 'yes';
		}
		//get Checkout via AJAX
		$.post( quick_checkout.ajax_url,
			{
				action                : 'quick_checkout_action',
				clear_cart            : clear_cart_option,
				checkout_add_variation: true, //this triggers the 'checkout_add_variation' if statement in 'quick_checkout_action'
				product_id            : quick_checkout.product_id,
				variation_id          : quick_checkout.product_variation_id,
				quantity              : quick_checkout.product_quantity,
				get_checkout          : true
			},

			function ( response ) {

				var checkout = $( response ).find( 'form.checkout' );
				$( checkout ).find( '.shop_table, #payment' ).remove();

				//Ensure create account is displayed for subscription products
				if ( quick_checkout.product_type === 'subscription' ) {
					$( '#createaccount' ).prop( 'checked', true ); //check create account
					$( 'p.create-account' ).hide(); //hide create account option checkbox container
					$( 'div.create-account' ).show(); //show div
				}

				//Get WooCommerce Checkout Script and execute to refresh AJAX
				quick_checkout_handle_ajax_response( response );


			} ).fail(function () {
				alert( "Error opening Quick Checkout" );
			} ).always( function () {
				console.log( 'AJAX quick_checkout_action complete' );
			} );


	}


	/**
	 * Center element horizontal and vertically
	 *
	 * Helper function
	 * http://stackoverflow.com/questions/4790475/jquery-programmatically-center-elements
	 *
	 * @param element
	 * @param xPosFromCenter
	 * @param yPosFromCenter
	 */
	function center_div( element, xPosFromCenter, yPosFromCenter ) {


		//shop pages x/y
		var xPos = parseInt( element.closest( '.product' ).find( '.wp-post-image' ).outerWidth() ) / 2 - parseInt( element.outerWidth() ) / 2 - xPosFromCenter;
		var yPos = parseInt( element.closest( '.product' ).find( '.wp-post-image' ).outerHeight() ) / 2 - parseInt( element.outerHeight() ) / 2 - yPosFromCenter;


		//Product pages x/y vars centering
		//modified selector is only difference
		if ( element.hasClass( 'quick-checkout-button-overlay-single' ) ) {
			xPos = parseInt( element.closest( '.images' ).find( '.wp-post-image' ).outerWidth() ) / 2 - parseInt( element.outerWidth() ) / 2 - xPosFromCenter;
			yPos = parseInt( element.closest( '.images' ).find( '.wp-post-image' ).outerHeight() ) / 2 - parseInt( element.outerHeight() ) / 2 - yPosFromCenter;
		}

		element.css( {top: yPos, left: xPos} );

	}


	/**
	 * Open Modal
	 */
	function qc_open_modal() {

		//Add style classes to quick checkout
		$( '#quick-checkout' ).addClass( 'white-popup' );

		// Open directly via API
		$.magnificPopup.open( {
			items    : {
				src : $( '#quick-checkout' ), // can be a HTML string, jQuery object, or CSS selector
				type: 'inline'
			},
			callbacks: {
				open : function () {
					//Fade in checkout
					$( '#quick-checkout' ).fadeIn( 'fast' );
					//Remove loading from clicked button parent
					$( '.quick-checkout-active-loading' ).removeClass( 'quick-checkout-active-loading' );
				},
				close: function () {
					//Remove popup class
					$( '#quick-checkout' ).removeClass( 'white-popup' );
					//Display none checkout
					$( '#quick-checkout' ).css( 'display', 'none' );

				}

			}
		} );

	}


	/**
	 * Handle Quick Cart AJAX Response
	 */
	function quick_checkout_handle_ajax_response( response ) {
		var new_order_review = $( response ).find( '#order_review' );
		$( '#order_review' ).empty().append( new_order_review );
		$( 'body' ).trigger( 'init_checkout' ); //initiate checkout b/c we reloaded w/ AJAX
		$( 'body' ).trigger( 'update_checkout' ); //trigger update checkout
	}

	/**
	 * On Window Load
	 *
	 * The following functions fire after the DOM window has loaded.
	 *
	 */

	$( window ).load( function () {

		//Center Hover shop buttons on hover
		$( '.quick-checkout-button-image-overlay' ).each( function ( index, element ) {
			center_div( $( element ), 0, 0 );
		} );

		//Fix for Checkout JS hiding login form
		$( '.woocommerce-account form.login' ).css( 'display', 'block' );


	} );

	//Remove single product default submit button if necessary
	$( '.quick-checkout-product-replace' ).prevAll( '.single_add_to_cart_button' ).remove();

	/**
	 * Hide buy now if variation product not in stock
	 * @since: 1.5.6
	 */
	$( 'body' ).on( 'found_variation', function ( event, variation ) {
		// wc_add_to_cart_variation_params is required to continue, ensure the object exists
		if ( typeof wc_add_to_cart_variation_params === 'undefined' )
			return false;
		if ( !variation.is_purchasable || !variation.is_in_stock || !variation.variation_is_visible ) {
			$( '.quick-checkout-product' ).hide();
		} else {
			$( '.quick-checkout-product' ).show();
		}
	} );

}( jQuery ));