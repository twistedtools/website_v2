<?php
/**
 * Plugin Name: WooCommerce - Show order IDs
 * Plugin URI: http://www.remicorson.com/easily-find-woocommerce-order-id/
 * Description: Adds a new columns to order list page to display order IDs
 * Version: 1.0
 * Author: Remi Corson
 * Author URI: http://remicorson.com
 * Requires at least: 3.5
 * Tested up to: 3.5
 *
 * Text Domain: -
 * Domain Path: -
 *
 */
 
/*
|--------------------------------------------------------------------------
| WooCommerce Order Extra Columns
|--------------------------------------------------------------------------
*/
 
/**
 * Load Custom Order Columns
 *
 * @access      public
 * @since       1.0 
 * @return      
*/
function woo_order_extra_columns($columns)
{
 
  $newcolumns = array(
		"cb"       		=> "<input type  = \"checkbox\" />",
		"order_ID"    => esc_html__('ID', 'woocommerce'),
	);
 
	$columns = array_merge($newcolumns, $columns);
	
	return $columns;
}
add_filter("manage_edit-shop_order_columns", "woo_order_extra_columns");
 
 
/**
 * Charge Order Columns Content
 *
 * @access      public
 * @since       1.0 
 * @return      
*/
function woo_order_extra_columns_content($column)
{
	global $post;
	
	$order_id = $post->ID;
 
	switch ($column)
	{
		case "order_ID":
			echo $order_id;
		break;	
		
	}
}
add_action("manage_posts_custom_column",  "woo_order_extra_columns_content");